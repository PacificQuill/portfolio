﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; //added for save
using System.Drawing.Imaging;
//using System.Threading.Thread;

namespace Fractal_Attempt
{
    public partial class MandelBrot_Fractal : Form
    {

        public MandelBrot_Fractal()
        {
            //Method Finished

            InitializeComponent();
            init();
            start();
            

        }
        public bool Negated
        {
            get { return checkBox1.Checked; }
            set { checkBox1.Checked = value; }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            Graphics windowG = e.Graphics;
            windowG.DrawImageUnscaled(picture, 0, 0); 
            Graphics g2 = e.Graphics; // Graphics (layer above the first one)
            if (rectangle)
            {
                Pen boxPen = new Pen(Color.White);
                Brush boxBrush = new SolidBrush(Color.FromArgb(100, 00, 255, 00));
                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        g2.DrawRectangle(boxPen, xs, ys, (xe - xs), (ye - ys));
                        g2.FillRectangle(boxBrush, xs, ys, (xe - xs), (ye - ys));
                    }
                    else
                    {
                        g2.DrawRectangle(boxPen, xs, ye, (xe - xs), (ys - ye));
                        g2.FillRectangle(boxBrush, xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        g2.DrawRectangle(boxPen, xe, ys, (xs - xe), (ye - ys));
                        g2.FillRectangle(boxBrush, xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        g2.DrawRectangle(boxPen, xe, ye, (xs - xe), (ys - ye));
                        g2.FillRectangle(boxBrush, xe, ye, (xs - xe), (ys - ye));
                    }
                }
            }


        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private Color DiffCol = Color.Black;

        


        private int numberofzooms = 0;


        private static float xy;
        private Bitmap picture;
        private Bitmap r;
        private Graphics g;

        private void button1_Click(object sender, EventArgs e)
        {

            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Filter = "Images (*.bmp)|*.bmp";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                int width = pictureBox1.Width;
                int height = pictureBox1.Height;
                Bitmap bmp = new Bitmap(width, height);
                pictureBox1.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));
                bmp.Save(dialog.FileName, ImageFormat.Bmp);

                string path = "lastopenpic.txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                    TextWriter tw = new StreamWriter(path,true);
                    tw.WriteLine(dialog.FileName);
                    tw.Close();

                    path = Path.GetFileNameWithoutExtension(dialog.FileName) + ".txt";


                    if (!File.Exists(path))
                    {
                        File.Create(path).Close();
                        writeInstance(path);
                        
                    }
                    else if (File.Exists(path))
                    {
                        File.Delete(path);
                        File.Create(path).Close();
                        writeInstance(path);
                    }

                }
                else if (File.Exists(path))
                {

                    TextWriter tw = new StreamWriter(path,true);
                    tw.WriteLine(dialog.FileName);
                    tw.WriteLine(Path.GetFileNameWithoutExtension(dialog.FileName));
                    tw.Close();

                    path = Path.GetFileNameWithoutExtension(dialog.FileName)+".txt";


                    if (!File.Exists(path))
                    {
                        File.Create(path).Close();
                        writeInstance(path);
                    }
                    else if (File.Exists(path))
                    {
                        File.Delete(path);
                        File.Create(path).Close();
                        writeInstance(path);
                    }


                }
             

            }

            dialog.Dispose();

        }
        public void writeInstance(String path)
        {
            TextWriter tw2 = new StreamWriter(path, true);
            tw2.WriteLine(x1.ToString());
            tw2.WriteLine(y1.ToString());
            tw2.WriteLine(xs.ToString());
            tw2.WriteLine(ys.ToString());
            tw2.WriteLine(xe.ToString());
            tw2.WriteLine(ye.ToString());
            tw2.WriteLine(xstart.ToString());
            tw2.WriteLine(ystart.ToString());
            tw2.WriteLine(xende.ToString());
            tw2.WriteLine(yende.ToString());
            tw2.WriteLine(xzoom.ToString());
            tw2.WriteLine(yzoom.ToString());
            tw2.WriteLine(xy.ToString());
            tw2.WriteLine(action.ToString());
            tw2.WriteLine(rectangle.ToString());
            tw2.WriteLine(finished.ToString());
            tw2.WriteLine(DiffCol.A.ToString());
            tw2.WriteLine(DiffCol.R.ToString());
            tw2.WriteLine(DiffCol.G.ToString());
            tw2.WriteLine(DiffCol.B.ToString());
            tw2.WriteLine(Negated.ToString());
            tw2.Close();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            ///////////Allow user to select a file to open.
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Please select the fractal bitmap you would like to load in";
            ofd.Filter = "Bitmap (*.bmp)|*.bmp";
            if (ofd.ShowDialog() == DialogResult.OK) //If user opened a file...
            {
                string FileToOpen =  ofd.FileName;
              //  picture = new Bitmap(FileToOpen);  
              // Two preceeding lines will display the bitmap as a static image
                FileToOpen = Path.GetFileNameWithoutExtension(ofd.FileName) + ".txt";
                System.IO.StreamReader reader = new System.IO.StreamReader(FileToOpen);
               string[] values = (reader.ReadToEnd()).Split('\n');
               x1 = Int32.Parse(values[0]);
               y1 = Int32.Parse(values[1]);
               xs = Int32.Parse(values[2]);
               ys = Int32.Parse(values[3]);
               xe = Int32.Parse(values[4]);
               ye = Int32.Parse(values[5]);
               xstart = Double.Parse(values[6]);
               ystart = Double.Parse(values[7]);
               xende = Double.Parse(values[8]);
               yende = Double.Parse(values[9]);
               xzoom = Double.Parse(values[10]);
               yzoom = Double.Parse(values[11]);
               xy = float.Parse(values[12]);
               action = bool.Parse(values[13]);
               rectangle = bool.Parse(values[14]);
               finished = bool.Parse(values[15]);
               DiffCol = Color.FromArgb(Int32.Parse(values[16]), Int32.Parse(values[17]), Int32.Parse(values[18]), Int32.Parse(values[19]));
               Negated = (bool.Parse(values[20]));
                
                panel1.BackColor = DiffCol;
               mandelbrot();
               Refresh();
               numberofzooms = 0;
               noOfZoomsLabel.Text = numberofzooms.ToString();
               noOfZoomsLabel.Refresh();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            init();
            start();
            numberofzooms = 0;
            noOfZoomsLabel.Text = numberofzooms.ToString();
            noOfZoomsLabel.Refresh();
            ticklabel.Text = "";
            ticklabel.Refresh();
            Refresh();
        }


        private void button4_MouseClick(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }

        private Cursor c1, c2;

        private void button5_MouseClick(object sender, MouseEventArgs e)
        {
            ColorDialog ColourPicker = new ColorDialog(); 
            ColourPicker.FullOpen = true ;
            if (ColourPicker.ShowDialog() == DialogResult.OK)
            {
                label1.Text = "Colour Has been accepted, Chosen Colour was: " + ColourPicker.Color.ToString();
                DiffCol = ColourPicker.Color;
                panel1.BackColor = DiffCol;
                mandelbrot();
                Refresh();
            }
        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get { return h; }
            }

            public float S
            {
                get { return s; }
            }

            public float B
            {
                get { return b; }
            }

            public int A
            {
                get { return a; }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;

                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        public Color Negate(Color Color)
        {
            int a=Color.A, r=Color.R, g=Color.G,b=Color.B, d;
            r = 255 - r;
            g = 255 - g;
            b = 255 - b;
            Color = Color.FromArgb(a, r, g, b);
            return Color;
        }

        

        private bool rup;
        private bool gup;
        private bool bup;

        private Color Cycle(Color Color)
        {
            int r = Color.R, g = Color.G, b = Color.B;
            if (r <= 10)
            {
                rup = true;
            }
            if (g <= 10)
            {
                gup = true;
            }
            if (b <= 10)
            {
                bup = true;
            }

            if (r >= 245)
            {
                rup = false;
            }
            if (g >= 245)
            {
                gup = false;
            }
            if (b >= 245)
            {
                bup = false;
            }
            if (rup == true)
            {
                r = r + 10;
            }
            else
            {
                r = r - 10;
            }
            if (gup == true)
            {
                g = g + 10;
            }
            else
            {
                g = g - 10;
            }
            if (bup == true)
            {
                b = b + 10;
            }
            else
            {
                b = b - 10;
            }

            Color = Color.FromArgb(255, r, g, b);
            return Color;

        }
        
int ticker=1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ticker < 48)
            {
                ticker++;
                ticklabel.Text = ticker.ToString();
                ticklabel.Refresh();
               
                using (var bmpTemp = new Bitmap("ColourCycle" + ticker + ".bmp"))
                {
                    picture = new Bitmap(bmpTemp);
                }
               
                pictureBox1.Image = picture;
                pictureBox1.Refresh();
                GC.Collect();
            }
            else
            {
                ticker = 1;
                ticklabel.Text = ticker.ToString();
                ticklabel.Refresh();

                using (var bmpTemp = new Bitmap("ColourCycle" + ticker + ".bmp"))
                {
                    picture = new Bitmap(bmpTemp);
                }
                
                pictureBox1.Image = picture;
                pictureBox1.Refresh();
                GC.Collect();
            }
        }

        private void button6_MouseClick(object sender, MouseEventArgs e)
        {
            label101.Text = "Rendering Colour Cycle please wait.";
            label101.Refresh();
            button7.Text = "View Cycle";
            button7.Refresh();
            for (int i = 1; i < 49; i++)
            {
                File.Delete("ColourCycle" + i + ".bmp");
                DiffCol = Cycle(DiffCol);
                mandelbrot();
               // Refresh();
               // Bitmap bmp = new Bitmap(640, 480);
               // pictureBox1.DrawToBitmap(bmp, new Rectangle(0, 0, 640, 480));
                picture.Save("ColourCycle" + i + ".bmp", ImageFormat.Bmp);
                GC.Collect();
            }
            label101.Text = "Colour Cycle has generated.";
            label101.Refresh();

        }

        int tickmark =1;

        private void button7_Click(object sender, EventArgs e)
        {
            if (ticker==tickmark) {
                timer1.Start();
                button7.Text = "Pause";
                button7.Refresh();
            } else
            {
                timer1.Stop();
                tickmark = ticker;
                button7.Text = "Restart";
                button7.Refresh();
            }
        }

        private void checkBox1_MouseClick(object sender, MouseEventArgs e) //Negation (handled elsewhere)
        {
            mandelbrot();
            Refresh();
        }

        public void init() // Instances prepared: Graphics and bitmap initialised.
        {
            //setSize(640, 480); SIH- Set in form designer.
            finished = false;
            //c1 = new Cursor(Cursor.WAIT_CURSOR); SIH - Succeeding 2 lines set 2 cursor objects.
            c1 = Cursors.WaitCursor;
            c2 = Cursors.Cross;
            x1 = pictureBox1.Width; // SIH -  Equivalent of Java getSize.width / height.
            y1 = pictureBox1.Height;
            x1label.Text = x1.ToString();
            x1label.Refresh();
            y1label.Text = y1.ToString();
            y1label.Refresh();
            xy = (float)x1 / (float)y1;
            xylabel.Text = xy.ToString();
            xylabel.Refresh();
            picture = new Bitmap(640, 480);
            r = new Bitmap(640, 480);
            Graphics g2 = Graphics.FromImage(r);
            g = Graphics.FromImage(picture); // get the graphics contact so that we can draw on the bitmap
            finished = true;
        }


        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            xzoomlabel.Text = xzoom.ToString();
            xzoomlabel.Refresh();
            yzoom = (yende - ystart) / (double)y1;
            yzoomlabel.Text = yzoom.ToString();
            yzoomlabel.Refresh();

            mandelbrot();
        }

        public void stop()
        {
        }

        public void paint(Graphics g)
        {
            update(g);
        }

        public void update(Graphics g)
        {
        }

        private void mandelbrot() // calculate and colour time!
        {
            Pen myPen = new Pen(Color.Black, 1);
            int x, y;
            float h, b, alt = 0.0f;
            action = false;
            this.Cursor = c1;
            label1.Text = "Status: Mandelbrot-Set will be produced - please wait...";// SIH - Replaced Applet Status bar with a label
            label1.Refresh();
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y);
                    if (h != alt)
                    {
                        b = 1.0f - h * h;
                        Color col = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));
                        col = AddColours(col, DiffCol);
                        if (checkBox1.Checked)
                        {
                            col = Negate(col);
                        }
                        myPen = new Pen(col);
                        alt = h;
                    }
                    g.DrawLine(myPen, x, y, x + 1, y);
                }
            label1.Text = "Status: Mandelbrot-Set ready - please select zoom area with pressed mouse."; // SIH - Replaced Applet Status bar with a label
            label1.Refresh();
            // setCursor(c2); SIH - CHANGED HERE FOR CURSOR
            this.Cursor = c2;
            action = true;
        }

        private Color AddColours(Color HSBCol, Color AdditiveCol)
        {
            Color NewColour;
            int a;
            int r;
            int g;
            int b;
            int difference;

            a = HSBCol.A + AdditiveCol.A;
            r = HSBCol.R + AdditiveCol.R;
            g = HSBCol.G + AdditiveCol.G;
            b = HSBCol.B + AdditiveCol.B;
            if (a > 255)
            {
                a = 255;
            }
            if (r > 255)
            {
                difference = r - 255;
                r = 255 - difference;
            }
            if (g > 255)
            {
                difference = g - 255;
                g = 255 - difference;
            }
            if (b > 255)
            {
                difference = b - 255;
                b = 255 - difference;
            }

            NewColour = Color.FromArgb(a, r, g, b);

            return NewColour;
        }



        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }


        private void initvalues() // reset start values
        {
            xstart = SX;
            xstartlabel.Text = xstart.ToString();
            xstartlabel.Refresh();
            ystart = SY;
            ystartlabel.Text = ystart.ToString();
            ystartlabel.Refresh();
            xende = EX;
            xendelabel.Text = xende.ToString();
            xendelabel.Refresh();
            yende = EY;
            yendelabel.Text = yende.ToString();
            yendelabel.Refresh();
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }


        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xe = e.X;
                xelabel.Text = xe.ToString();
                xelabel.Refresh();
                ye = e.Y;
                yelabel.Text = ye.ToString();
                yelabel.Refresh();
            }
            Refresh();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {

            // e.consume();
            if (action)
            {
                xs = e.X;
                xslabel.Text = xs.ToString();
                xslabel.Refresh();
                ys = e.Y;
                yslabel.Text = ys.ToString();
                yslabel.Refresh();
            }
            rectangle = true;
            numberofzooms++;
            noOfZoomsLabel.Text = numberofzooms.ToString();
            noOfZoomsLabel.Refresh();
            Refresh();
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            if (action)
            {
                xe = e.X;
                xslabel2.Text = xe.ToString();
                xslabel2.Refresh();
                ye = e.Y;
                yslabel2.Text = ye.ToString();
                yslabel2.Refresh();
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2))
                {
                    initvalues();
                    numberofzooms = 0;
                    noOfZoomsLabel.Text = numberofzooms.ToString();
                    noOfZoomsLabel.Refresh();
                }
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                if (checkBox3.Checked)
                {
                    for (double i = 1; i > 0; i = i - 0.25)
                    {
                        xzoom = ((((xende * i)) - ((xstart * i))) / (double)x1);
                        xzoomlabel.Text = xzoom.ToString();
                        xzoomlabel.Refresh();
                        yzoom = ((((yende*i)) - ((ystart*i))) / (double)y1);
                        yzoomlabel.Text = yzoom.ToString();
                        yzoomlabel.Refresh();
                        mandelbrot();
                        rectangle = false;
                        Refresh();
                    }
                }
                else
                {
                    xzoom = (xende - xstart) / (double)x1;
                    xzoomlabel.Text = xzoom.ToString();
                    xzoomlabel.Refresh();
                    yzoom = (yende - ystart) / (double)y1;
                    yzoomlabel.Text = yzoom.ToString();
                    yzoomlabel.Refresh();
                    mandelbrot();
                    rectangle = false;
                    Refresh();
                }
            }
        }

    }
}
