﻿namespace Fractal_Attempt
{
    partial class MandelBrot_Fractal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MandelBrot_Fractal));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.x1label = new System.Windows.Forms.Label();
            this.y1label = new System.Windows.Forms.Label();
            this.xslabel = new System.Windows.Forms.Label();
            this.yslabel = new System.Windows.Forms.Label();
            this.xelabel = new System.Windows.Forms.Label();
            this.yelabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.xylabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.xstartlabel = new System.Windows.Forms.Label();
            this.ystartlabel = new System.Windows.Forms.Label();
            this.xendelabel = new System.Windows.Forms.Label();
            this.yendelabel = new System.Windows.Forms.Label();
            this.xzoomlabel = new System.Windows.Forms.Label();
            this.yzoomlabel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.yslabel2 = new System.Windows.Forms.Label();
            this.xslabel2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.noOfZoomsLabel = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label101 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ticklabel = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(640, 480);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Tomato;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(665, 175);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 31);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save as...";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(665, 205);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 31);
            this.button2.TabIndex = 2;
            this.button2.Text = "Load a Bitmap";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(665, 236);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(88, 31);
            this.button3.TabIndex = 3;
            this.button3.Text = "Refresh";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(12, 485);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Status: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(765, 277);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Width = ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(762, 293);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Height = ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(692, 306);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Zoom Start (x) = ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(692, 322);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Zoom Start (y) =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(709, 339);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Current X Position = ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(709, 354);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Current Y Position = ";
            // 
            // x1label
            // 
            this.x1label.AutoSize = true;
            this.x1label.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.x1label.Location = new System.Drawing.Point(814, 277);
            this.x1label.Name = "x1label";
            this.x1label.Size = new System.Drawing.Size(0, 13);
            this.x1label.TabIndex = 12;
            // 
            // y1label
            // 
            this.y1label.AutoSize = true;
            this.y1label.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.y1label.Location = new System.Drawing.Point(814, 293);
            this.y1label.Name = "y1label";
            this.y1label.Size = new System.Drawing.Size(0, 13);
            this.y1label.TabIndex = 13;
            // 
            // xslabel
            // 
            this.xslabel.AutoSize = true;
            this.xslabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xslabel.Location = new System.Drawing.Point(779, 307);
            this.xslabel.Name = "xslabel";
            this.xslabel.Size = new System.Drawing.Size(13, 13);
            this.xslabel.TabIndex = 14;
            this.xslabel.Text = "0";
            // 
            // yslabel
            // 
            this.yslabel.AutoSize = true;
            this.yslabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.yslabel.Location = new System.Drawing.Point(779, 322);
            this.yslabel.Name = "yslabel";
            this.yslabel.Size = new System.Drawing.Size(13, 13);
            this.yslabel.TabIndex = 15;
            this.yslabel.Text = "0";
            // 
            // xelabel
            // 
            this.xelabel.AutoSize = true;
            this.xelabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xelabel.Location = new System.Drawing.Point(814, 338);
            this.xelabel.Name = "xelabel";
            this.xelabel.Size = new System.Drawing.Size(0, 13);
            this.xelabel.TabIndex = 16;
            // 
            // yelabel
            // 
            this.yelabel.AutoSize = true;
            this.yelabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.yelabel.Location = new System.Drawing.Point(814, 352);
            this.yelabel.Name = "yelabel";
            this.yelabel.Size = new System.Drawing.Size(0, 13);
            this.yelabel.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(732, 367);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Aspect Ratio = ";
            // 
            // xylabel
            // 
            this.xylabel.AutoSize = true;
            this.xylabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xylabel.Location = new System.Drawing.Point(814, 367);
            this.xylabel.Name = "xylabel";
            this.xylabel.Size = new System.Drawing.Size(0, 13);
            this.xylabel.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(716, 380);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Real Value Start = ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(698, 394);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Complex Value Start = ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(719, 407);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Real Value End = ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(701, 421);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Complex Value End = ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(756, 434);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "X Zoom = ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(756, 448);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Y Zoom = ";
            // 
            // xstartlabel
            // 
            this.xstartlabel.AutoSize = true;
            this.xstartlabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xstartlabel.Location = new System.Drawing.Point(810, 380);
            this.xstartlabel.Name = "xstartlabel";
            this.xstartlabel.Size = new System.Drawing.Size(41, 13);
            this.xstartlabel.TabIndex = 26;
            this.xstartlabel.Text = "label15";
            // 
            // ystartlabel
            // 
            this.ystartlabel.AutoSize = true;
            this.ystartlabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ystartlabel.Location = new System.Drawing.Point(810, 394);
            this.ystartlabel.Name = "ystartlabel";
            this.ystartlabel.Size = new System.Drawing.Size(41, 13);
            this.ystartlabel.TabIndex = 27;
            this.ystartlabel.Text = "label16";
            // 
            // xendelabel
            // 
            this.xendelabel.AutoSize = true;
            this.xendelabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xendelabel.Location = new System.Drawing.Point(810, 408);
            this.xendelabel.Name = "xendelabel";
            this.xendelabel.Size = new System.Drawing.Size(41, 13);
            this.xendelabel.TabIndex = 28;
            this.xendelabel.Text = "label17";
            // 
            // yendelabel
            // 
            this.yendelabel.AutoSize = true;
            this.yendelabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.yendelabel.Location = new System.Drawing.Point(810, 421);
            this.yendelabel.Name = "yendelabel";
            this.yendelabel.Size = new System.Drawing.Size(41, 13);
            this.yendelabel.TabIndex = 29;
            this.yendelabel.Text = "label18";
            // 
            // xzoomlabel
            // 
            this.xzoomlabel.AutoSize = true;
            this.xzoomlabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xzoomlabel.Location = new System.Drawing.Point(810, 435);
            this.xzoomlabel.Name = "xzoomlabel";
            this.xzoomlabel.Size = new System.Drawing.Size(41, 13);
            this.xzoomlabel.TabIndex = 30;
            this.xzoomlabel.Text = "label19";
            // 
            // yzoomlabel
            // 
            this.yzoomlabel.AutoSize = true;
            this.yzoomlabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.yzoomlabel.Location = new System.Drawing.Point(810, 448);
            this.yzoomlabel.Name = "yzoomlabel";
            this.yzoomlabel.Size = new System.Drawing.Size(41, 13);
            this.yzoomlabel.TabIndex = 31;
            this.yzoomlabel.Text = "label20";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(685, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(232, 169);
            this.pictureBox2.TabIndex = 32;
            this.pictureBox2.TabStop = false;
            // 
            // yslabel2
            // 
            this.yslabel2.AutoSize = true;
            this.yslabel2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.yslabel2.Location = new System.Drawing.Point(897, 322);
            this.yslabel2.Name = "yslabel2";
            this.yslabel2.Size = new System.Drawing.Size(13, 13);
            this.yslabel2.TabIndex = 36;
            this.yslabel2.Text = "0";
            // 
            // xslabel2
            // 
            this.xslabel2.AutoSize = true;
            this.xslabel2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xslabel2.Location = new System.Drawing.Point(897, 307);
            this.xslabel2.Name = "xslabel2";
            this.xslabel2.Size = new System.Drawing.Size(13, 13);
            this.xslabel2.TabIndex = 35;
            this.xslabel2.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(810, 322);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Zoom End (y) =";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Location = new System.Drawing.Point(810, 306);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Zoom End (x) = ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(709, 461);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(103, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Number of Zooms = ";
            // 
            // noOfZoomsLabel
            // 
            this.noOfZoomsLabel.AutoSize = true;
            this.noOfZoomsLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.noOfZoomsLabel.Location = new System.Drawing.Point(810, 461);
            this.noOfZoomsLabel.Name = "noOfZoomsLabel";
            this.noOfZoomsLabel.Size = new System.Drawing.Size(13, 13);
            this.noOfZoomsLabel.TabIndex = 38;
            this.noOfZoomsLabel.Text = "0";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Tomato;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Location = new System.Drawing.Point(752, 236);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(92, 31);
            this.button4.TabIndex = 39;
            this.button4.Text = "Exit";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button4_MouseClick);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(752, 175);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(92, 31);
            this.button5.TabIndex = 40;
            this.button5.Text = "Adjust Colour";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button5_MouseClick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(847, 177);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(31, 25);
            this.panel1.TabIndex = 41;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button5_MouseClick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBox1.Location = new System.Drawing.Point(883, 180);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(61, 17);
            this.checkBox1.TabIndex = 42;
            this.checkBox1.Text = "Negate";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.checkBox1_MouseClick);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBox3.Location = new System.Drawing.Point(847, 243);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(94, 17);
            this.checkBox3.TabIndex = 44;
            this.checkBox3.Text = "Animate Zoom";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label101.Location = new System.Drawing.Point(15, 502);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(0, 13);
            this.label101.TabIndex = 45;
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ticklabel
            // 
            this.ticklabel.AutoSize = true;
            this.ticklabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ticklabel.ForeColor = System.Drawing.Color.Red;
            this.ticklabel.Location = new System.Drawing.Point(591, 434);
            this.ticklabel.Name = "ticklabel";
            this.ticklabel.Size = new System.Drawing.Size(0, 39);
            this.ticklabel.TabIndex = 46;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(752, 205);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(92, 31);
            this.button6.TabIndex = 47;
            this.button6.Text = "Generate Cycle";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button6_MouseClick);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(845, 205);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(96, 31);
            this.button7.TabIndex = 48;
            this.button7.Text = "View Cycle";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // MandelBrot_Fractal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(947, 523);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.ticklabel);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.noOfZoomsLabel);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.yslabel2);
            this.Controls.Add(this.xslabel2);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.yzoomlabel);
            this.Controls.Add(this.xzoomlabel);
            this.Controls.Add(this.yendelabel);
            this.Controls.Add(this.xendelabel);
            this.Controls.Add(this.ystartlabel);
            this.Controls.Add(this.xstartlabel);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.xylabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.yelabel);
            this.Controls.Add(this.xelabel);
            this.Controls.Add(this.yslabel);
            this.Controls.Add(this.xslabel);
            this.Controls.Add(this.y1label);
            this.Controls.Add(this.x1label);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MandelBrot_Fractal";
            this.Text = "C3413087 - Sam Hoole - Mandelbrot A.S.E.A Ass. 1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label x1label;
        private System.Windows.Forms.Label y1label;
        private System.Windows.Forms.Label xslabel;
        private System.Windows.Forms.Label yslabel;
        private System.Windows.Forms.Label xelabel;
        private System.Windows.Forms.Label yelabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label xylabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label xstartlabel;
        private System.Windows.Forms.Label ystartlabel;
        private System.Windows.Forms.Label xendelabel;
        private System.Windows.Forms.Label yendelabel;
        private System.Windows.Forms.Label xzoomlabel;
        private System.Windows.Forms.Label yzoomlabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label yslabel2;
        private System.Windows.Forms.Label xslabel2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label noOfZoomsLabel;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label ticklabel;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}

