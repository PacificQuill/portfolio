A selection of a few of my projects from University.

ASEA Assignment 2 - This was a bug reporting system developed during the course. This will not work without the MySQL server I configured on my own machine,
		    however, it was a sizable piece of work, and thought it was worth sharing.

ASEB Project      - This was the cycle computer output processing application I built in the final Semester, files have been provided in the "Input Documents for ASEBProject"
		    Directory. To open file, navigate to "Home" Tab, and follow the "Open File..." dialogue box. Alternatively, if you navigate to the "Calendar View"
		    Tab, There is a button to import the whole folder, reading the date of each file from it's header; This will limit the dates shown in the Calendar view
		    to only the range of dates for which files exist (Earliest workout -> Latest workout). Dates with an associated file appear BOLD, and can be clicked to select,
		    before enabling the "Open" Button.
		    This will allow you to Browse the data by traversing the other tabs.
		    N.B.I   - Additional Metrics calculated on the Home tab requires the user to input their own FTP (Functional Threshhold Power) before submission and calculation.
		    N.B.II  - Interval data is not calculated on load. To calculate, and to populate the Interval Data tab, Click "Find Intervals" on the Graphs Tab.
		    N.B.III - If unclear; A subset of the graph may be calculated by clicking and dragging on the graph, and then clicking "Generate Chart from Selected Subset".

Fractal Attempt   - This was the one translated from a Java applet and then developed upon to test how well we could translate,familiarise and improve upon existing code.			
		    Nothing really to say for it, have a play!

Thanks for looking,
Sam.