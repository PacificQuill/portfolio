﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace ASEBProject
{
    public partial class Form1 : Form
    {
        bool hasassociated;
        int indexofassociated;
        public int versionno;
        public String lineholder;
        public List<string> filelines = new List<string>();
        public List<string> Calendarfiles = new List<string>();
        public List<string> mainbody = new List<string>();
        public List<int> HRHeartRate = new List<int>();
        public List<float> HRSpeed = new List<float>();
        public List<int> HRCadence = new List<int>();
        public List<int> HRAltitude = new List<int>();
        public List<int> HRPower = new List<int>();
        public List<int> HRPowerBalance = new List<int>();
        public List<int> AboveAverage = new List<int>();
        public List<int> intervalstartindex = new List<int>();
        public List<int> intervalendindex = new List<int>();
        public List<int> intervals = new List<int>();
        public List<DateTime> datetimelist = new List<DateTime>();
        public List<DateTime> calendardatesoffiles = new List<DateTime>();
        public int startofselection;
        public int endofselection;
        public List<int> SelectedRange = new List<int>();
        public string dayoffile;
        public string monthoffile;
        public string yearoffile;
        public string sthour;
        public string stminute;
        public string stsec;
        public string sttenthsec;
        public string tthour;
        public string ttminute;
        public string ttsec;
        public string tttenthsec;
        public bool ismainbody;
        public int powaaa;
        public int fulllength;
        public float avgspeed;
        public TimeSpan starttime;
        public DateTime Date;
        public int interval;
        public const float MILESRATIO = 0.6213712F;
        public double normalisedpower;
        public double intensityfactor;
        public double trainingstressscore;
        bool intervalsdrawn = false;
        public Boolean iszoomed;

        //################################################################################################################################################################################//

        public Form1()
        {
            InitializeComponent();
            btn_open_assoc_file.Enabled = false;
            if (File.Exists("Calendar.txt"))
            {
                readincalendar();
            }
        }

        //################################################################################################################################################################################//

        private void btn_open_file_Click(object sender, EventArgs e)
        {
            String line;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Please select the .hrm doc you would like to view.";
            ofd.Filter = "HRM (*.hrm)|*.hrm";
            ismainbody = false;
            filelines.Clear();
            mainbody.Clear();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {   // Open the text file using a stream reader.

                    using (StreamReader sr = new StreamReader(ofd.FileName))
                    {
                        // Read the stream to a string, and write the string to the console.
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (ismainbody)
                            {
                                mainbody.Add(line.Trim());
                            }
                            else
                            {
                                filelines.Add(line.Trim());
                                if (line.Trim() == "[HRData]")
                                {
                                    ismainbody = true;
                                }
                            }

                        }
                    }
                    clearallcharts();
                    select_from_params();
                    splitHR();
                    getaveragespeed(0, fulllength);
                    getmaxspeed(0, fulllength);
                    getavgHR(0, fulllength);
                    getminHR(0, fulllength);
                    getmaxHR(0, fulllength);
                    getmaxpower(0, fulllength);
                    getavgpower(0, fulllength);
                    getaveragealtitude(0, fulllength);
                    getdistancetravelled();
                    getmaxalt(0, fulllength);
                    createdatelist();
                    populateinteractivegraph();
                    enableallseries();
                    populatedatatable();
                    calculateNormalisedPower();
                }
                catch (Exception e2)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e2.Message);
                }

            }
            createDGVIntervalData();
        }

        //################################################################################################################################################################################//

        public void select_from_params()
        {
            String[] split;
            for (int i = 0; i < filelines.Count - 1; i++)
            {
                split = filelines[i].Split('=');
                if (split[0] == "Version")
                {
                    lblversionout.Text = split[1];
                }
                else if (split[0] == "Date")
                {
                    yearoffile = split[1].Substring(0, 4);
                    monthoffile = split[1].Substring(4, 2);
                    dayoffile = split[1].Substring(6);
                    lbldatetext.Text = returndate(yearoffile, monthoffile, dayoffile);
                    Date = new DateTime(Int32.Parse(yearoffile), Int32.Parse(monthoffile), Int32.Parse(dayoffile));
                }
                else if (split[0] == "StartTime")
                {
                    string[] digits = split[1].Split(':');
                    sthour = digits[0];
                    stminute = digits[1];
                    string[] digits2 = digits[2].Split('.');
                    stsec = digits2[0];
                    sttenthsec = digits2[1];
                    string suffix;
                    if ((Int32.Parse(sthour)) >= 12)
                    {
                        suffix = "pm";
                    }
                    else
                    {
                        suffix = "am";
                    }
                    lbltimeout.Text = sthour + ":" + stminute + ":" + stsec + "." + sttenthsec + " " + suffix;
                    starttime = new TimeSpan(Int32.Parse(sthour), Int32.Parse(stminute), Int32.Parse(stsec));
                }
                else if (split[0] == "Length")
                {
                    string[] digits = split[1].Split(':');
                    tthour = digits[0];
                    ttminute = digits[1];
                    string[] digits2 = digits[2].Split('.');
                    ttsec = digits2[0];
                    tttenthsec = digits2[1];
                    lbldurationout.Text = tthour + "Hours, " + ttminute + "Minutes, " + ttsec + "." + tttenthsec + "Seconds.";

                }
                else if (split[0] == "Interval")
                {
                    interval = Int32.Parse(split[1].ToString());
                    lblintervalout.Text = interval.ToString() + " second(s)";
                }


            }

        }

        //################################################################################################################################################################################//

        public void getdistancetravelled()
        {
            int hourstodec = ((Int32.Parse(tthour) * 3600) * 10);
            int minutestodec = ((Int32.Parse(ttminute) * 60) * 10);
            int secstodec = (Int32.Parse(ttsec) * 10);
            int tenthstodec = (Int32.Parse(tttenthsec));
            int tot = hourstodec + minutestodec + secstodec + tenthstodec;
            float tenthsectravel = avgspeed / 36000;
            float totaltravel = tenthsectravel * (tot);
            if (rdkm.Checked)
            {
                lbltotdistout.Text = Math.Round(totaltravel, 2).ToString() + "Km";
            }
            else
            {
                lbltotdistout.Text = Math.Round((totaltravel * MILESRATIO), 2).ToString() + "Miles";
            }

        }

        //################################################################################################################################################################################//

        public String getaveragespeed(int start, int end)
        {
            float sum = 0;
            int count = 0;
            for (int i = start; i < end; i++)
            {
                sum += HRSpeed[i];
                count++;
            }
            avgspeed = (float.Parse(sum.ToString()) / float.Parse(count.ToString()));
            if (rdkm.Checked)
            {
                lblavgspeedout.Text = Math.Round(avgspeed, 2).ToString() + "Km/h";
                return Math.Round(avgspeed, 2).ToString() + "Km/h";
            }
            else
            {
                lblavgspeedout.Text = Math.Round((avgspeed * MILESRATIO), 2).ToString() + "MPH";
                return Math.Round((avgspeed * MILESRATIO), 2).ToString() + "MPH";
            }

        }

        //################################################################################################################################################################################//

        public String getaveragealtitude(int start, int end)
        {
            int sum = 0;
            int count = 0;
            for (int i = start; i < end; i++)
            {
                sum += HRAltitude[i];
                count++;
            }
            lblavgaltout.Text = (sum / count).ToString();
            return (sum / count).ToString();
        }

        //################################################################################################################################################################################//

        public String getmaxalt(int start, int end)
        {
            int max = 0;
            for (int i = start; i < end; i++)
            {
                if (HRAltitude[i] > max)
                {
                    max = HRAltitude[i];
                }
            }
            lblmaxaltout.Text = max.ToString();
            return max.ToString();
        }

        //################################################################################################################################################################################//

        public String getmaxspeed(int start, int end)
        {
            float max = 0;
            for (int i = start; i < end; i++)
            {
                if (HRSpeed[i] > max)
                {
                    max = HRSpeed[i];
                }
            }
            if (rdkm.Checked)
            {
                lblmaxspeedout.Text = max.ToString() + "Km/h";
                return max.ToString() + "Km/h";
            }
            else
            {
                lblmaxspeedout.Text = (max * MILESRATIO).ToString() + "MPH";
                return (max * MILESRATIO).ToString() + "MPH";
            }
            
        }

        //################################################################################################################################################################################//

        public String getminHR(int start, int end)
        {
            int min = 1000;
            for (int i = start; i < end; i++)
            {
                if (HRHeartRate[i] < min)
                {
                    min = HRHeartRate[i];
                }
            }
            lblminhrout.Text = min.ToString();
            return min.ToString();
        }

        //################################################################################################################################################################################//

        public String getavgHR(int start, int end)
        {
            int sum = 0;
            int count = 0;
            for (int i = start; i < end; i++)
            {
                sum += HRHeartRate[i];
                count++;
            }
            lblavghrout.Text = (sum / count).ToString();
            return (sum / count).ToString();
        }

        //################################################################################################################################################################################//

        public String getmaxHR(int start, int end)
        {
            int max = 0;
            for (int i = start; i < end; i++)
            {
                if (HRHeartRate[i] > max)
                {
                    max = HRHeartRate[i];
                }
            }
            lblmaxhrout.Text = max.ToString();
            return max.ToString();
        }

        //################################################################################################################################################################################//

        public String getmaxpower(int start, int end)
        {
            int max = 0;
            for (int i = start; i < end; i++)
            {
                if (HRPower[i] > max)
                {
                    max = HRPower[i];
                }
            }
            lblmaxpowout.Text = max.ToString();
            return max.ToString();
        }

        //################################################################################################################################################################################//

        public String getavgpower(int start, int end)
        {
            int sum = 0;
            int count = 0;
            for (int i = start; i < end; i++)
            {
                sum += HRPower[i];
                count++;
            }
            lblavgpowout.Text = (sum / count).ToString();
            powaaa = (sum / count) / 2 + 43;
            return (sum / count).ToString();
        }

        //################################################################################################################################################################################//

        public void splitHR()
        {
            HRHeartRate.Clear();
            HRSpeed.Clear();
            HRCadence.Clear();
            HRPower.Clear();
            HRAltitude.Clear();
            HRPowerBalance.Clear();
            fulllength = 0;
            try
            {
                fulllength = mainbody.Count;
                string[] splitlist;
                for (int i = 0; i < fulllength; i++)
                {
                    splitlist = mainbody[i].Split(new char[0]);
                    HRHeartRate.Add(Int32.Parse(splitlist[0]));
                    HRSpeed.Add((float.Parse(splitlist[1])) / 10);
                    HRCadence.Add(Int32.Parse(splitlist[2]));
                    HRAltitude.Add(Int32.Parse(splitlist[3]));
                    HRPower.Add(Int32.Parse(splitlist[4]));
                    HRPowerBalance.Add(Int32.Parse(splitlist[5]));
                }
            }
            catch
            {
                fulllength = mainbody.Count;
                string[] splitlist;
                for (int i = 0; i < fulllength; i++)
                {
                    splitlist = mainbody[i].Split(new char[0]);
                    HRHeartRate.Add(Int32.Parse(splitlist[0]));
                    HRSpeed.Add((float.Parse(splitlist[1])) / 10);
                    HRCadence.Add(Int32.Parse(splitlist[2]));
                    HRAltitude.Add(Int32.Parse(splitlist[3]));
                    HRPower.Add(Int32.Parse(splitlist[4]));
                }
            }
        }

        //################################################################################################################################################################################//

        public string returndate(String Year, String Month, String Date)
        {
            string full = "";
            if (Date == "01")
            {
                full = full + Date + "st of ";
            }
            else if (Date == "02")
            {
                full = full + Date + "nd of ";
            }
            else if (Date == "03")
            {
                full = full + Date + "rd of ";
            }
            else
            {
                full = full + Date + "th of ";
            }
            if (Month == "01")
            {
                full = full + "January, ";
            }
            else if (Month == "02")
            {
                full = full + "February, ";
            }
            else if (Month == "03")
            {
                full = full + "March, ";
            }
            else if (Month == "04")
            {
                full = full + "April, ";
            }
            else if (Month == "05")
            {
                full = full + "May, ";
            }
            else if (Month == "06")
            {
                full = full + "June, ";
            }
            else if (Month == "07")
            {
                full = full + "July, ";
            }
            else if (Month == "08")
            {
                full = full + "August, ";
            }
            else if (Month == "09")
            {
                full = full + "September, ";
            }
            else if (Month == "10")
            {
                full = full + "October, ";
            }
            else if (Month == "11")
            {
                full = full + "November, ";

            }
            else if (Month == "12")
            {
                full = full + "December, ";
            }
            full = full + Year;
            return full;
        }

        //################################################################################################################################################################################//

        private void chk_power_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_power.Checked)
            {
                chart6.Series["Power"].Enabled = true;
            }
            else
            {
                chart6.Series["Power"].Enabled = false;
            }
        }

        //################################################################################################################################################################################//

        private void chk_HR_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_HR.Checked)
            {
                chart6.Series["HeartRate"].Enabled = true;
            }
            else
            {
                chart6.Series["HeartRate"].Enabled = false;
            }
        }

        //################################################################################################################################################################################//

        private void chk_Speed_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_Speed.Checked)
            {
                chart6.Series["Speed"].Enabled = true;
            }
            else
            {
                chart6.Series["Speed"].Enabled = false;
            }
        }

        //################################################################################################################################################################################//

        private void chk_cadence_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_cadence.Checked)
            {
                chart6.Series["Cadence"].Enabled = true;
            }
            else
            {
                chart6.Series["Cadence"].Enabled = false;
            }
        }

        //################################################################################################################################################################################//

        private void chk_altitude_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_altitude.Checked)
            {
                chart6.Series["Altitude"].Enabled = true;
            }
            else
            {
                chart6.Series["Altitude"].Enabled = false;
            }
        }

        //################################################################################################################################################################################//

        public void populateinteractivegraph()
        {
            for (int i = 0; i < fulllength; i++)
            {
                chart6.Series["Altitude"].Points.AddXY(datetimelist[i], HRAltitude[i]);
                chart6.Series["Cadence"].Points.AddXY(datetimelist[i], HRCadence[i]);
                chart6.Series["HeartRate"].Points.AddXY(datetimelist[i], HRHeartRate[i]);
                chart6.Series["Power"].Points.AddXY(datetimelist[i], HRPower[i]);
                chart6.Series["Speed"].Points.AddXY(datetimelist[i], HRSpeed[i]);
            }
            chart6.Series["Altitude"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Altitude Selected"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Altitude Selected"].BorderWidth = 3;

            chart6.Series["Cadence"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Cadence Selected"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Cadence Selected"].BorderWidth = 3;

            chart6.Series["HeartRate"].ChartType = SeriesChartType.FastLine;
            chart6.Series["HeartRate Selected"].ChartType = SeriesChartType.FastLine;
            chart6.Series["HeartRate Selected"].BorderWidth = 3;

            chart6.Series["Power"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Power Selected"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Power Selected"].BorderWidth = 3;

            chart6.Series["Speed"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Speed Selected"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Speed Selected"].BorderWidth = 3;

            chart6.Series["Altitude"].Enabled = false;
            chart6.Series["Cadence"].Enabled = false;
            chart6.Series["HeartRate"].Enabled = false;
            chart6.Series["Power"].Enabled = false;
            chart6.Series["Speed"].Enabled = false;
            chart6.Series["Intervals"].ChartType = SeriesChartType.FastLine;
            chart6.Series["Intervals"].BorderWidth = 5;
        }

        //################################################################################################################################################################################//

        public void populatedatatable()
        {
            DataTable dt = new DataTable();
            TimeSpan temptime;
            float x = 0;
            Date = Date.Add(starttime);
            dt.Columns.Add("Date / Time");
            dt.Columns.Add("Heart Rate (BPM)");
            if (rdkm.Checked)
            {
                dt.Columns.Add("Speed (Km/h)");
            }
            else
            {
                dt.Columns.Add("Speed (mph)");
            }
            dt.Columns.Add("Cadence(RPM)");
            dt.Columns.Add("Altitude(m)");
            dt.Columns.Add("Power(w)");
            for (int i = 0; i < fulllength; i++)
            {
                if (rdkm.Checked)
                {
                    x = HRSpeed[i];
                }
                else
                {
                    x = HRSpeed[i] * MILESRATIO;
                }
                temptime = new TimeSpan(0, 0, (i * interval));
                dt.Rows.Add(new object[] { Date.Add(temptime), HRHeartRate[i], x, HRCadence[i], HRAltitude[i], HRPower[i] });
            }
            dataGridView1.DataSource = dt;

        }

        //################################################################################################################################################################################//

        private void rdm_CheckedChanged(object sender, EventArgs e)
        {
            Date = new DateTime(Int32.Parse(yearoffile), Int32.Parse(monthoffile), Int32.Parse(dayoffile));
            populatedatatable();
            getdistancetravelled();
            getmaxspeed(0, fulllength);
            getaveragespeed(0, fulllength);
        }

        //################################################################################################################################################################################//

        private void rdkm_CheckedChanged(object sender, EventArgs e)
        {
            Date = new DateTime(Int32.Parse(yearoffile), Int32.Parse(monthoffile), Int32.Parse(dayoffile));
            populatedatatable();
            getdistancetravelled();
            getmaxspeed(0, fulllength);
            getaveragespeed(0, fulllength);
            createDGVIntervalData();
        }

        //################################################################################################################################################################################//

        public void clearallcharts()
        {
            chart6.Series["HeartRate"].Points.Clear();
            chart6.Series["Speed"].Points.Clear();
            chart6.Series["Cadence"].Points.Clear();
            chart6.Series["Altitude"].Points.Clear();
            chart6.Series["Power"].Points.Clear();
            chart6.Series["HeartRate Selected"].Points.Clear();
            chart6.Series["Speed Selected"].Points.Clear();
            chart6.Series["Cadence Selected"].Points.Clear();
            chart6.Series["Altitude Selected"].Points.Clear();
            chart6.Series["Power Selected"].Points.Clear();
            chart6.Series["Intervals"].Points.Clear();
            chart6.Refresh();
        }

        //################################################################################################################################################################################//

        public void calculateNormalisedPower()
        {
            float step1sum = 0f;
            float step1avg = 0f;
            double step3sum = 0f;
            double step3avg = 0f;
            double raiseto = 0f;
            List<float> step1vals = new List<float>();
            List<double> valsraisedpow4 = new List<double>();
            for (int i = 30; i < fulllength; i++)
            {
                step1sum = 0f;
                step1avg = 0f;
                for (int j = 30; j > 0; j--)
                {
                    step1sum += HRPower[i - j];
                }
                step1avg = step1sum / 30;
                step1vals.Add(step1avg);
            }
            //raise by ^4
            for (int i = 0; i < step1vals.Count; i++)
            {
                raiseto = Math.Pow(step1vals[i], 4);
                valsraisedpow4.Add(raiseto);
            }

            for (int i = 0; i < valsraisedpow4.Count; i++)
            {
                step3sum += valsraisedpow4[i];
            }
            step3avg = step3sum / valsraisedpow4.Count;
            normalisedpower = Math.Pow(step3avg, 1.0f / 4);
            lblnormpowout.Text = Math.Round(normalisedpower, 2).ToString() + "w";
        }

        //################################################################################################################################################################################//

        public void calculateIntensityFactor()
        {
            try {
                float FTP = float.Parse(lblftpin.Text);
                intensityfactor = normalisedpower / FTP;
                lblifout.Text = Math.Round(intensityfactor * 100, 2).ToString() + "%";
            } catch
            {
                MessageBox.Show("Please enter a valid FTP.");
            }
        }

        //################################################################################################################################################################################//

        public void calculateTrainingStressScore()
        {
            try { 
            double timeasdec;
            timeasdec = double.Parse(tthour);
            timeasdec += double.Parse(ttminute) / 60;
            timeasdec += double.Parse(ttsec) / 3600;
            timeasdec += double.Parse(ttsec) / 36000;
            trainingstressscore = Math.Pow((intensityfactor * intensityfactor), 2) * 100 * timeasdec;
            lbltssout.Text = Math.Round(trainingstressscore, 2).ToString() + "TSS";
        } catch
            {
                MessageBox.Show("Please enter a valid FTP.");
            }
}

        //################################################################################################################################################################################//

        private void button1_Click(object sender, EventArgs e)
        {
            calculateNormalisedPower();
            calculateIntensityFactor();
            calculateTrainingStressScore();
        }

        //################################################################################################################################################################################//

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //################################################################################################################################################################################//

        public void selectrange()
        {
            chart6.Series["HeartRate Selected"].Points.Clear();
            chart6.Series["Power Selected"].Points.Clear();
            chart6.Series["Speed Selected"].Points.Clear();
            chart6.Series["Altitude Selected"].Points.Clear();
            chart6.Series["Cadence Selected"].Points.Clear();
            // SelectedRange.Clear();
            // chart1.Series["HeartRate"].Points.Clear();
            for (int i = startofselection; i < endofselection; i++)
            {
                chart6.Series["HeartRate Selected"].Points.AddXY(datetimelist[i], HRHeartRate[i]);
                chart6.Series["Power Selected"].Points.AddXY(datetimelist[i], HRPower[i]);
                chart6.Series["Speed Selected"].Points.AddXY(datetimelist[i], HRSpeed[i]);
                chart6.Series["Altitude Selected"].Points.AddXY(datetimelist[i], HRAltitude[i]);
                chart6.Series["Cadence Selected"].Points.AddXY(datetimelist[i], HRCadence[i]);
            }
            chart6.Refresh();
        }

        //################################################################################################################################################################################//

        public void createdatelist()
        {
            datetimelist.Clear();
            Date = new DateTime(Int32.Parse(yearoffile), Int32.Parse(monthoffile), Int32.Parse(dayoffile));
            TimeSpan temptime;
            TimeSpan toConv;
            for (int i = 0; i < fulllength; i++)
            {
                temptime = new TimeSpan(0, 0, (i * interval));
                toConv = starttime.Add(temptime);
                datetimelist.Add(Date + toConv);
            }
        }

        //################################################################################################################################################################################//

        private void chart6_MouseDown(object sender, MouseEventArgs e)
        {
            if (!iszoomed)
            {
                int sweeper;
                int index = 0;
                for (int i = 0; i < 1000; i++)
                {
                    sweeper = Convert.ToInt32(e.Y) + i;
                    HitTestResult result = chart6.HitTest(e.X, sweeper);
                    if (result.ChartElementType == ChartElementType.DataPoint)
                    {
                        if (result.ChartElementType == ChartElementType.DataPoint)
                        {
                            if (result.Series.ToString().Contains("Selected"))
                            {
                                index = startofselection + result.PointIndex;
                            }
                            else
                            {
                                index = result.PointIndex;
                            }
                        }
                    }
                    sweeper = e.Y - i;
                    result = chart6.HitTest(e.X, sweeper);
                    if (result.ChartElementType == ChartElementType.DataPoint)
                    {
                        if (result.ChartElementType == ChartElementType.DataPoint)
                        {
                            if (result.Series.ToString().Contains("Selected"))
                            {
                                index = startofselection + result.PointIndex;
                            }
                            else
                            {
                                index = result.PointIndex;
                            }
                        }
                    }
                }
                startofselection = index;
            }
        }

        //################################################################################################################################################################################//

        private void chart6_MouseUp(object sender, MouseEventArgs e)
        {
            if (!iszoomed)
            {
                int sweeper;
                int index = 0;
                for (int i = 0; i < 1000; i++)
                {
                    sweeper = Convert.ToInt32(e.Y) + i;
                    HitTestResult result = chart6.HitTest(e.X, sweeper);

                    if (result.ChartElementType == ChartElementType.DataPoint)
                    {
                        if (result.Series.ToString().Contains("Selected"))
                        {
                            index = startofselection + result.PointIndex;
                        }
                        else
                        {
                            index = result.PointIndex;
                        }
                    }
                    sweeper = e.Y - i;
                    result = chart6.HitTest(e.X, sweeper);
                    if (result.ChartElementType == ChartElementType.DataPoint)
                    {
                        if (result.Series.ToString().Contains("Selected"))
                        {
                            index = startofselection + result.PointIndex;
                        }
                        else
                        {
                            index = result.PointIndex;
                        }
                    }
                }
                endofselection = index;
                selectrange();
            }
        }

        //################################################################################################################################################################################//

        private void button3_Click(object sender, EventArgs e)
        {
            if (endofselection - 5 > startofselection)
            {
                notificationlabel.Text = "";
                chart6.Series["Altitude"].Points.Clear();
                chart6.Series["Cadence"].Points.Clear();
                chart6.Series["HeartRate"].Points.Clear();
                chart6.Series["Power"].Points.Clear();
                chart6.Series["Speed"].Points.Clear();
                chart6.Series["Altitude Selected"].Points.Clear();
                chart6.Series["Cadence Selected"].Points.Clear();
                chart6.Series["HeartRate Selected"].Points.Clear();
                chart6.Series["Power Selected"].Points.Clear();
                chart6.Series["Speed Selected"].Points.Clear();
                chart6.Series["Intervals"].Points.Clear();
                for (int i = startofselection; i < endofselection; i++)
                {
                    chart6.Series["Altitude"].Points.AddXY(datetimelist[i], HRAltitude[i]);
                    chart6.Series["Cadence"].Points.AddXY(datetimelist[i], HRCadence[i]);
                    chart6.Series["HeartRate"].Points.AddXY(datetimelist[i], HRHeartRate[i]);
                    chart6.Series["Power"].Points.AddXY(datetimelist[i], HRPower[i]);
                    chart6.Series["Speed"].Points.AddXY(datetimelist[i], HRSpeed[i]);
                    chart6.Series["Altitude Selected"].Points.Clear();
                    chart6.Series["Cadence Selected"].Points.Clear();
                    chart6.Series["HeartRate Selected"].Points.Clear();
                    chart6.Series["Power Selected"].Points.Clear();
                    chart6.Series["Speed Selected"].Points.Clear();
                }
                chart6.Refresh();
                iszoomed = true;
                getaveragespeed(startofselection, endofselection);
                getmaxspeed(startofselection, endofselection);
                getavgHR(startofselection, endofselection);
                getminHR(startofselection, endofselection);
                getmaxHR(startofselection, endofselection);
                getmaxpower(startofselection, endofselection);
                getavgpower(startofselection, endofselection);
                getaveragealtitude(startofselection, endofselection);
                getdistancetravelled();
                getmaxalt(startofselection, endofselection);

            }
            else
            {
                notificationlabel.Text = "Please select a range \n of values before trying to \n generate a graph of them";
            }
        }

        //################################################################################################################################################################################//

        private void button4_Click(object sender, EventArgs e)
        {
            clearallcharts();
            populateinteractivegraph();
            iszoomed = false;
            getaveragespeed(0, fulllength);
            getmaxspeed(0, fulllength);
            getavgHR(0, fulllength);
            getminHR(0, fulllength);
            getmaxHR(0, fulllength);
            getmaxpower(0, fulllength);
            getavgpower(0, fulllength);
            getaveragealtitude(0, fulllength);
            getdistancetravelled();
            getmaxalt(0, fulllength);
            enableallseries();
        }

        private void enableallseries()
        {
            chart6.Series["Altitude"].Enabled = true;
            chart6.Series["Speed"].Enabled = true;
            chart6.Series["Power"].Enabled = true;
            chart6.Series["Cadence"].Enabled = true;
            chart6.Series["HeartRate"].Enabled = true;
            chk_altitude.Checked = true;
            chk_cadence.Checked = true;
            chk_HR.Checked = true;
            chk_power.Checked = true;
            chk_Speed.Checked = true;
        }

        //################################################################################################################################################################################//

        private void button5_Click(object sender, EventArgs e)
        {
            intervalstartindex.Clear();
            intervalendindex.Clear();
            AboveAverage.Clear();
            for (int i = 0; i < fulllength; i++)
            {
                if (HRPower[i] > powaaa)
                {
                    AboveAverage.Add(i);
                }
            }
            for (int i = 0; i < AboveAverage.Count() - 1; i++)
            {
                if (i == 0)
                {
                    intervalstartindex.Add(AboveAverage[i]);

                }
                else if (i == (AboveAverage.Count() - 2))
                {
                    intervalendindex.Add(AboveAverage[i]);

                }
                else
                {
                    if ((AboveAverage[i - 1] == (AboveAverage[i] - 1)) && (AboveAverage[i + 1] == (AboveAverage[i] + 1)))
                    {

                    }
                    else {
                        if ((AboveAverage[i - 1] != (AboveAverage[i] - 1)))
                        {
                            if ((AboveAverage[i + 1] != (AboveAverage[i] + 1)))
                            {
                                intervalstartindex.Add(AboveAverage[i]);
                                intervalendindex.Add(AboveAverage[i]);
                            }
                            else if ((AboveAverage[i + 1] == (AboveAverage[i] + 1)))
                            {
                                intervalstartindex.Add(AboveAverage[i]);
                            }
                        }
                        else
                        {
                            if ((AboveAverage[i + 1] != (AboveAverage[i] + 1)))
                            {
                                intervalendindex.Add(AboveAverage[i]);
                            }
                        }
                    }

                }
            }

            for (int i = 0; i < intervalstartindex.Count(); i++)
            {
                if (intervalstartindex[i] == intervalendindex[i])
                {
                    intervalstartindex.RemoveAt(i);
                    intervalendindex.RemoveAt(i);
                    i--;
                }
                else if (intervalstartindex[i] > (intervalendindex[i] - 12))
                {
                    intervalstartindex.RemoveAt(i);
                    intervalendindex.RemoveAt(i);
                    i--;
                }
            }
            DrawIntervals();
            createDGVIntervalData();
        }

        //################################################################################################################################################################################//

        public void DrawIntervals()
        {
            string outputintervals = "--------INTERVALS--------\n";
            if (!intervalsdrawn)
            {
                for (int i = 0; i < intervalstartindex.Count(); i++)
                {
                    for (int j = intervalstartindex[i]; j < intervalendindex[i]; j++)
                    {
                        chart6.Series["Intervals"].Points.AddXY(datetimelist[j], HRPower[j]);
                    }
                    if ((i + 1) < intervalstartindex.Count())
                    {
                        for (int j = intervalendindex[i] + 1; j < intervalstartindex[i + 1]; j++)
                        {
                            chart6.Series["Intervals"].Points.AddXY(datetimelist[j], 0);
                        }
                    }
                }
                intervalsdrawn = true;

            }
            for (int i = 0; i < intervalstartindex.Count(); i++)
            {
                outputintervals = outputintervals + "Interval " + i.ToString() + " is between " + datetimelist[intervalstartindex[i]].ToString("HH:mm:ss tt") + " and " + datetimelist[intervalendindex[i]].ToString("HH:mm:ss tt") + "\n";
            }
            rtb_int_out.Text = outputintervals;
        }

        //################################################################################################################################################################################//

      

        //################################################################################################################################################################################//

        private void button6_Click(object sender, EventArgs e)
        {
            File.Delete("Calendar.txt");

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            try
            {
                string[] files = Directory.GetFiles(fbd.SelectedPath);
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Contains(".hrm"))
                    {
                        Calendarfiles.Add(files[i]);
                        importdate(files[i]);
                    }
                }
            }
            catch (Exception x)
            {
                MessageBox.Show("Error ...:" + x.Message);
            }
            using (StreamWriter writetext = new StreamWriter("Calendar.txt"))
            {
                for (int i = 0; i < Calendarfiles.Count(); i++)
                {
                    writetext.WriteLine(Calendarfiles[i] + "#" + calendardatesoffiles[i].ToString());
                }
            }
            readincalendar();
        }

        //################################################################################################################################################################################//

        public void importdate(String filepath)
        {
            String line;
            ismainbody = false;
            mainbody.Clear();
            using (StreamReader sr = new StreamReader(filepath))
            {
                // Read the stream to a string, and write the string to the console.
                while ((line = sr.ReadLine()) != null)
                {
                    if (ismainbody)
                    {
                        mainbody.Add(line.Trim());
                    }
                    else
                    {
                        filelines.Add(line.Trim());
                        if (line.Trim() == "[HRData]")
                        {
                            ismainbody = true;
                        }
                    }

                }
            }
            String[] split;
            for (int i = 0; i < filelines.Count - 1; i++)
            {
                split = filelines[i].Split('=');
                if (split[0] == "Date")
                {
                    yearoffile = split[1].Substring(0, 4);
                    monthoffile = split[1].Substring(4, 2);
                    dayoffile = split[1].Substring(6);
                    lbldatetext.Text = returndate(yearoffile, monthoffile, dayoffile);
                    Date = new DateTime(Int32.Parse(yearoffile), Int32.Parse(monthoffile), Int32.Parse(dayoffile));
                }
            }
            calendardatesoffiles.Add(Date);
            filelines.Clear();
        }

        //################################################################################################################################################################################//

        public void readincalendar()
        {
            calendardatesoffiles.Clear();
            Calendarfiles.Clear();
            string line;
            string[] split;
            string[] split2;

            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader("Calendar.txt");
            while ((line = file.ReadLine()) != null)
            {
                split = line.Split('#');
                Calendarfiles.Add(split[0]);
                split2 = (split[1].Substring(0, 10)).Split('/');
                yearoffile = split2[2];
                monthoffile = split2[1];
                dayoffile = split2[0];
                Date = new DateTime(Int32.Parse(yearoffile), Int32.Parse(monthoffile), Int32.Parse(dayoffile));
                calendardatesoffiles.Add(Date);
            }
            for (int i = 0; i < calendardatesoffiles.Count(); i++)
            {
                monthCalendar1.AddBoldedDate(calendardatesoffiles[i]);
            }
            file.Close();
            setearliestdate();
            setlatestdate();
            monthCalendar1.Refresh();
        }

        //################################################################################################################################################################################//

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            hasassociated = false;

            for (int i = 0; i < calendardatesoffiles.Count(); i++)
            {
                if (monthCalendar1.SelectionRange.Start.ToShortDateString() == calendardatesoffiles[i].ToShortDateString())
                {
                    hasassociated = true;
                    indexofassociated = i;
                }
            }
            if (hasassociated)
            {
                label6.Text = "Workout on " + monthCalendar1.SelectionRange.Start.ToShortDateString() + " Selected";
                lbl_output_assoc_file.Text = "File Associated : "+Calendarfiles[indexofassociated];
                btn_open_assoc_file.Enabled = true;
            }
            else
            {
                label6.Text = "";
                lbl_output_assoc_file.Text = "This date has no associated file.";
                btn_open_assoc_file.Enabled = false;
            }
        }

        //################################################################################################################################################################################//

        public void setearliestdate()
        {
            DateTime holder = DateTime.Now;
            for (int i = 0; i < calendardatesoffiles.Count(); i++)
            {
                int result = DateTime.Compare(holder, calendardatesoffiles[i]);
                if (result > 0)
                {
                    holder = calendardatesoffiles[i];
                }
            }
            Console.WriteLine(holder.ToString());
            monthCalendar1.MinDate = holder;
        }

        //################################################################################################################################################################################//

        public void setlatestdate()
        {
            DateTime holder = new DateTime(2002, 4, 20);
            for (int i = 0; i < calendardatesoffiles.Count(); i++)
            {
                int result = DateTime.Compare(holder, calendardatesoffiles[i]);
                if (result < 0)
                {
                    holder = calendardatesoffiles[i];
                }
            }
            Console.WriteLine(holder.ToString());
            monthCalendar1.MaxDate = holder;
        }

        //################################################################################################################################################################################//

        private void btn_open_assoc_file_Click(object sender, EventArgs e)
        {
            String line;
            ismainbody = false;
            filelines.Clear();
            mainbody.Clear();
                try
                {   // Open the text file using a stream reader.

                    using (StreamReader sr = new StreamReader(Calendarfiles[indexofassociated]))
                    {
                        // Read the stream to a string, and write the string to the console.
                        while ((line = sr.ReadLine()) != null)
                        {
                            if (ismainbody)
                            {
                                mainbody.Add(line.Trim());
                            }
                            else
                            {
                                filelines.Add(line.Trim());
                                if (line.Trim() == "[HRData]")
                                {
                                    ismainbody = true;
                                }
                            }

                        }
                    }
                    clearallcharts();
                    select_from_params();
                    splitHR();
                    getaveragespeed(0, fulllength);
                    getmaxspeed(0, fulllength);
                    getavgHR(0, fulllength);
                    getminHR(0, fulllength);
                    getmaxHR(0, fulllength);
                    getmaxpower(0, fulllength);
                    getavgpower(0, fulllength);
                    getaveragealtitude(0, fulllength);
                    getdistancetravelled();
                    getmaxalt(0, fulllength);
                    createdatelist();
                    populateinteractivegraph();
                    enableallseries();
                    populatedatatable();
                    calculateNormalisedPower();
                monthCalendar1.Refresh();
                chart6.Refresh();
                }
                catch (Exception e2)
                {
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e2.Message);
                }
            createDGVIntervalData();
        }

        //################################################################################################################################################################################//

        private void createDGVIntervalData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Interval No.");//
            dt.Columns.Add("Start Time");//
            dt.Columns.Add("End Time");//
            dt.Columns.Add("Duration");//
            dt.Columns.Add("Max Speed");//
            dt.Columns.Add("Avg. Speed");//
            dt.Columns.Add("Max HR");
            dt.Columns.Add("Avg. HR");
            dt.Columns.Add("Minx HR");
            dt.Columns.Add("Max Power");
            dt.Columns.Add("Avg. Power");
            dt.Columns.Add("Max Alt");
            dt.Columns.Add("Avg Alt");
            for (int i = 0; i < intervalendindex.Count(); i++)
            {
                TimeSpan dur = datetimelist[intervalendindex[i]]- datetimelist[intervalstartindex[i]];
                dt.Rows.Add(new object[] {
                    i,
                    datetimelist[intervalstartindex[i]],
                    datetimelist[intervalendindex[i]],
                    dur,
                    getmaxspeed(intervalstartindex[i],intervalendindex[i]),
                    getaveragespeed(intervalstartindex[i],intervalendindex[i]),
                    getmaxHR(intervalstartindex[i],intervalendindex[i]),
                    getavgHR(intervalstartindex[i],intervalendindex[i]),
                    getminHR(intervalstartindex[i],intervalendindex[i]),
                    getmaxpower(intervalstartindex[i],intervalendindex[i]),
                    getavgpower(intervalstartindex[i],intervalendindex[i]),
                    getmaxalt(intervalstartindex[i],intervalendindex[i]),
                    getaveragealtitude(intervalstartindex[i],intervalendindex[i])
                });
            }
            dgvints.DataSource = dt;
        }
    }
}

