﻿namespace ASEBProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.StatisticsTab = new System.Windows.Forms.TabPage();
            this.panel16 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblftpin = new System.Windows.Forms.TextBox();
            this.lbltssout = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblifout = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblnormpowout = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbldurationout = new System.Windows.Forms.Label();
            this.lblintervalout = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rdm = new System.Windows.Forms.RadioButton();
            this.lbltimeout = new System.Windows.Forms.Label();
            this.rdkm = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbldatetext = new System.Windows.Forms.Label();
            this.lbltotdistout = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblversionout = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_open_file = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.notificationlabel = new System.Windows.Forms.Label();
            this.chart6 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtb_int_out = new System.Windows.Forms.RichTextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lblavgaltout = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblmaxaltout = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblavgpowout = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lblavghrout = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblmaxpowout = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblavgspeedout = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblmaxhrout = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblminhrout = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lblmaxspeedout = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chk_power = new System.Windows.Forms.CheckBox();
            this.chk_altitude = new System.Windows.Forms.CheckBox();
            this.chk_HR = new System.Windows.Forms.CheckBox();
            this.chk_cadence = new System.Windows.Forms.CheckBox();
            this.chk_Speed = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_open_assoc_file = new System.Windows.Forms.Button();
            this.lbl_output_assoc_file = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.button6 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvints = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.StatisticsTab.SuspendLayout();
            this.panel16.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel18.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvints)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.StatisticsTab);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1130, 707);
            this.tabControl1.TabIndex = 31;
            // 
            // StatisticsTab
            // 
            this.StatisticsTab.Controls.Add(this.panel16);
            this.StatisticsTab.Controls.Add(this.panel15);
            this.StatisticsTab.Location = new System.Drawing.Point(4, 22);
            this.StatisticsTab.Name = "StatisticsTab";
            this.StatisticsTab.Padding = new System.Windows.Forms.Padding(3);
            this.StatisticsTab.Size = new System.Drawing.Size(1122, 681);
            this.StatisticsTab.TabIndex = 0;
            this.StatisticsTab.Text = "Home";
            this.StatisticsTab.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.groupBox2);
            this.panel16.Controls.Add(this.label1);
            this.panel16.Controls.Add(this.lbldurationout);
            this.panel16.Controls.Add(this.lblintervalout);
            this.panel16.Controls.Add(this.label5);
            this.panel16.Controls.Add(this.rdm);
            this.panel16.Controls.Add(this.lbltimeout);
            this.panel16.Controls.Add(this.rdkm);
            this.panel16.Controls.Add(this.label7);
            this.panel16.Controls.Add(this.label4);
            this.panel16.Controls.Add(this.label3);
            this.panel16.Controls.Add(this.label9);
            this.panel16.Controls.Add(this.lbldatetext);
            this.panel16.Controls.Add(this.lbltotdistout);
            this.panel16.Controls.Add(this.label2);
            this.panel16.Controls.Add(this.lblversionout);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(3, 533);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1116, 145);
            this.panel16.TabIndex = 48;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblftpin);
            this.groupBox2.Controls.Add(this.lbltssout);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.lblifout);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lblnormpowout);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(600, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(334, 160);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Advanced Metrics";
            // 
            // lblftpin
            // 
            this.lblftpin.Location = new System.Drawing.Point(168, 53);
            this.lblftpin.Name = "lblftpin";
            this.lblftpin.Size = new System.Drawing.Size(100, 20);
            this.lblftpin.TabIndex = 57;
            // 
            // lbltssout
            // 
            this.lbltssout.AutoSize = true;
            this.lbltssout.Location = new System.Drawing.Point(167, 111);
            this.lbltssout.Name = "lbltssout";
            this.lbltssout.Size = new System.Drawing.Size(0, 13);
            this.lbltssout.TabIndex = 56;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 110);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 13);
            this.label22.TabIndex = 55;
            this.label22.Text = "Training Stress Score:";
            // 
            // lblifout
            // 
            this.lblifout.AutoSize = true;
            this.lblifout.Location = new System.Drawing.Point(167, 86);
            this.lblifout.Name = "lblifout";
            this.lblifout.Size = new System.Drawing.Size(0, 13);
            this.lblifout.TabIndex = 54;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 53;
            this.label13.Text = "Intensity Factor:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "FTP (w):";
            // 
            // lblnormpowout
            // 
            this.lblnormpowout.AutoSize = true;
            this.lblnormpowout.Location = new System.Drawing.Point(167, 33);
            this.lblnormpowout.Name = "lblnormpowout";
            this.lblnormpowout.Size = new System.Drawing.Size(0, 13);
            this.lblnormpowout.TabIndex = 50;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 49;
            this.label8.Text = "Normalised Power: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Software Version:";
            // 
            // lbldurationout
            // 
            this.lbldurationout.AutoSize = true;
            this.lbldurationout.Location = new System.Drawing.Point(313, 85);
            this.lbldurationout.Name = "lbldurationout";
            this.lbldurationout.Size = new System.Drawing.Size(0, 13);
            this.lbldurationout.TabIndex = 10;
            // 
            // lblintervalout
            // 
            this.lblintervalout.AutoSize = true;
            this.lblintervalout.Location = new System.Drawing.Point(313, 48);
            this.lblintervalout.Name = "lblintervalout";
            this.lblintervalout.Size = new System.Drawing.Size(0, 13);
            this.lblintervalout.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(940, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Display distances and speeds as:";
            // 
            // rdm
            // 
            this.rdm.AutoSize = true;
            this.rdm.Location = new System.Drawing.Point(943, 48);
            this.rdm.Name = "rdm";
            this.rdm.Size = new System.Drawing.Size(49, 17);
            this.rdm.TabIndex = 41;
            this.rdm.Text = "Miles";
            this.rdm.UseVisualStyleBackColor = true;
            this.rdm.CheckedChanged += new System.EventHandler(this.rdm_CheckedChanged);
            // 
            // lbltimeout
            // 
            this.lbltimeout.AutoSize = true;
            this.lbltimeout.Location = new System.Drawing.Point(313, 67);
            this.lbltimeout.Name = "lbltimeout";
            this.lbltimeout.Size = new System.Drawing.Size(0, 13);
            this.lbltimeout.TabIndex = 9;
            // 
            // rdkm
            // 
            this.rdkm.AutoSize = true;
            this.rdkm.Checked = true;
            this.rdkm.Location = new System.Drawing.Point(1031, 48);
            this.rdkm.Name = "rdkm";
            this.rdkm.Size = new System.Drawing.Size(73, 17);
            this.rdkm.TabIndex = 42;
            this.rdkm.TabStop = true;
            this.rdkm.Text = "Kilometers";
            this.rdkm.UseVisualStyleBackColor = true;
            this.rdkm.CheckedChanged += new System.EventHandler(this.rdkm_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(132, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Stats recorded every: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(132, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Duration of Training session:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(132, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Time of Training Session:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(132, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Total distance travelled:";
            // 
            // lbldatetext
            // 
            this.lbldatetext.AutoSize = true;
            this.lbldatetext.Location = new System.Drawing.Point(313, 29);
            this.lbldatetext.Name = "lbldatetext";
            this.lbldatetext.Size = new System.Drawing.Size(0, 13);
            this.lbldatetext.TabIndex = 6;
            // 
            // lbltotdistout
            // 
            this.lbltotdistout.AutoSize = true;
            this.lbltotdistout.Location = new System.Drawing.Point(313, 104);
            this.lbltotdistout.Name = "lbltotdistout";
            this.lbltotdistout.Size = new System.Drawing.Size(0, 13);
            this.lbltotdistout.TabIndex = 28;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(132, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Date Of File creation:";
            // 
            // lblversionout
            // 
            this.lblversionout.AutoSize = true;
            this.lblversionout.Location = new System.Drawing.Point(313, 11);
            this.lblversionout.Name = "lblversionout";
            this.lblversionout.Size = new System.Drawing.Size(0, 13);
            this.lblversionout.TabIndex = 4;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.button2);
            this.panel15.Controls.Add(this.button1);
            this.panel15.Controls.Add(this.btn_open_file);
            this.panel15.Controls.Add(this.dataGridView1);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(3, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1116, 675);
            this.panel15.TabIndex = 47;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(969, 454);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 21);
            this.button2.TabIndex = 45;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(969, 397);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 23);
            this.button1.TabIndex = 44;
            this.button1.Text = "Calc Adv. Metrics";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_open_file
            // 
            this.btn_open_file.Location = new System.Drawing.Point(969, 426);
            this.btn_open_file.Name = "btn_open_file";
            this.btn_open_file.Size = new System.Drawing.Size(138, 23);
            this.btn_open_file.TabIndex = 0;
            this.btn_open_file.Text = "Open File...";
            this.btn_open_file.UseVisualStyleBackColor = true;
            this.btn_open_file.Click += new System.EventHandler(this.btn_open_file_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(713, 449);
            this.dataGridView1.TabIndex = 39;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tabControl2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1122, 681);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Graphs";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1116, 675);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Controls.Add(this.groupBox1);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1108, 649);
            this.tabPage7.TabIndex = 5;
            this.tabPage7.Text = "Interactive";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.notificationlabel);
            this.panel14.Controls.Add(this.chart6);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(3, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1102, 426);
            this.panel14.TabIndex = 9;
            // 
            // notificationlabel
            // 
            this.notificationlabel.AutoSize = true;
            this.notificationlabel.Location = new System.Drawing.Point(947, 276);
            this.notificationlabel.Name = "notificationlabel";
            this.notificationlabel.Size = new System.Drawing.Size(0, 13);
            this.notificationlabel.TabIndex = 6;
            // 
            // chart6
            // 
            this.chart6.BackHatchStyle = System.Windows.Forms.DataVisualization.Charting.ChartHatchStyle.BackwardDiagonal;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.Title = "Time";
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.chart6.ChartAreas.Add(chartArea1);
            this.chart6.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart6.Legends.Add(legend1);
            this.chart6.Location = new System.Drawing.Point(0, 0);
            this.chart6.Name = "chart6";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Power";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "HeartRate";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Speed";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Cadence";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Altitude";
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Power Selected";
            series6.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series7.ChartArea = "ChartArea1";
            series7.Legend = "Legend1";
            series7.Name = "HeartRate Selected";
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series8.ChartArea = "ChartArea1";
            series8.Legend = "Legend1";
            series8.Name = "Speed Selected";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "Cadence Selected";
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series10.ChartArea = "ChartArea1";
            series10.Legend = "Legend1";
            series10.Name = "Altitude Selected";
            series10.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series11.ChartArea = "ChartArea1";
            series11.Legend = "Legend1";
            series11.Name = "Intervals";
            series11.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            this.chart6.Series.Add(series1);
            this.chart6.Series.Add(series2);
            this.chart6.Series.Add(series3);
            this.chart6.Series.Add(series4);
            this.chart6.Series.Add(series5);
            this.chart6.Series.Add(series6);
            this.chart6.Series.Add(series7);
            this.chart6.Series.Add(series8);
            this.chart6.Series.Add(series9);
            this.chart6.Series.Add(series10);
            this.chart6.Series.Add(series11);
            this.chart6.Size = new System.Drawing.Size(1102, 426);
            this.chart6.TabIndex = 3;
            this.chart6.Text = "chart6";
            this.chart6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chart6_MouseDown);
            this.chart6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chart6_MouseUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.chk_power);
            this.groupBox1.Controls.Add(this.chk_altitude);
            this.groupBox1.Controls.Add(this.chk_HR);
            this.groupBox1.Controls.Add(this.chk_cadence);
            this.groupBox1.Controls.Add(this.chk_Speed);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(3, 429);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1102, 217);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select to view";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rtb_int_out);
            this.groupBox3.Controls.Add(this.panel17);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(147, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(952, 198);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Statistics for this";
            // 
            // rtb_int_out
            // 
            this.rtb_int_out.Location = new System.Drawing.Point(448, 16);
            this.rtb_int_out.Name = "rtb_int_out";
            this.rtb_int_out.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtb_int_out.Size = new System.Drawing.Size(285, 147);
            this.rtb_int_out.TabIndex = 47;
            this.rtb_int_out.Text = "";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.button5);
            this.panel17.Controls.Add(this.button4);
            this.panel17.Controls.Add(this.button3);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel17.Location = new System.Drawing.Point(749, 16);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(200, 179);
            this.panel17.TabIndex = 7;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(12, 108);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(173, 39);
            this.button5.TabIndex = 6;
            this.button5.Text = "Find Intervals";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 62);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(173, 39);
            this.button4.TabIndex = 5;
            this.button4.Text = "Return to Original Graph";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(173, 39);
            this.button3.TabIndex = 4;
            this.button3.Text = "Generate Chart from selected subset";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(23, 142);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Altitude";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(22, 119);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Power";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(21, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 13);
            this.label19.TabIndex = 44;
            this.label19.Text = "Heart Rate";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 70);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 43;
            this.label18.Text = "Speed";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(229, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 42;
            this.label17.Text = "Maximum";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(346, 43);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 13);
            this.label16.TabIndex = 41;
            this.label16.Text = "Average";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(117, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 40;
            this.label14.Text = "Minimum";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(83, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(353, 101);
            this.panel1.TabIndex = 39;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.lblavgaltout);
            this.panel10.Location = new System.Drawing.Point(235, 73);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(110, 21);
            this.panel10.TabIndex = 43;
            // 
            // lblavgaltout
            // 
            this.lblavgaltout.AutoSize = true;
            this.lblavgaltout.Location = new System.Drawing.Point(3, 2);
            this.lblavgaltout.Name = "lblavgaltout";
            this.lblavgaltout.Size = new System.Drawing.Size(0, 13);
            this.lblavgaltout.TabIndex = 26;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.lblmaxaltout);
            this.panel6.Location = new System.Drawing.Point(119, 73);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(110, 21);
            this.panel6.TabIndex = 39;
            // 
            // lblmaxaltout
            // 
            this.lblmaxaltout.AutoSize = true;
            this.lblmaxaltout.Location = new System.Drawing.Point(3, 3);
            this.lblmaxaltout.Name = "lblmaxaltout";
            this.lblmaxaltout.Size = new System.Drawing.Size(0, 13);
            this.lblmaxaltout.TabIndex = 30;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.lblavgpowout);
            this.panel11.Location = new System.Drawing.Point(235, 49);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(110, 21);
            this.panel11.TabIndex = 42;
            // 
            // lblavgpowout
            // 
            this.lblavgpowout.AutoSize = true;
            this.lblavgpowout.Location = new System.Drawing.Point(3, 4);
            this.lblavgpowout.Name = "lblavgpowout";
            this.lblavgpowout.Size = new System.Drawing.Size(0, 13);
            this.lblavgpowout.TabIndex = 22;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Maroon;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Location = new System.Drawing.Point(3, 73);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(110, 21);
            this.panel5.TabIndex = 35;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel12.Controls.Add(this.lblavghrout);
            this.panel12.Location = new System.Drawing.Point(235, 26);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(110, 21);
            this.panel12.TabIndex = 41;
            // 
            // lblavghrout
            // 
            this.lblavghrout.AutoSize = true;
            this.lblavghrout.Location = new System.Drawing.Point(3, 3);
            this.lblavghrout.Name = "lblavghrout";
            this.lblavghrout.Size = new System.Drawing.Size(0, 13);
            this.lblavghrout.TabIndex = 16;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.lblmaxpowout);
            this.panel7.Location = new System.Drawing.Point(119, 49);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(110, 21);
            this.panel7.TabIndex = 38;
            // 
            // lblmaxpowout
            // 
            this.lblmaxpowout.AutoSize = true;
            this.lblmaxpowout.Location = new System.Drawing.Point(3, 3);
            this.lblmaxpowout.Name = "lblmaxpowout";
            this.lblmaxpowout.Size = new System.Drawing.Size(0, 13);
            this.lblmaxpowout.TabIndex = 24;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Controls.Add(this.lblavgspeedout);
            this.panel13.Location = new System.Drawing.Point(235, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(110, 21);
            this.panel13.TabIndex = 40;
            // 
            // lblavgspeedout
            // 
            this.lblavgspeedout.AutoSize = true;
            this.lblavgspeedout.Location = new System.Drawing.Point(3, 3);
            this.lblavgspeedout.Name = "lblavgspeedout";
            this.lblavgspeedout.Size = new System.Drawing.Size(0, 13);
            this.lblavgspeedout.TabIndex = 12;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Maroon;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Location = new System.Drawing.Point(3, 49);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(110, 21);
            this.panel4.TabIndex = 34;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.lblmaxhrout);
            this.panel8.Location = new System.Drawing.Point(119, 26);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(110, 21);
            this.panel8.TabIndex = 37;
            // 
            // lblmaxhrout
            // 
            this.lblmaxhrout.AutoSize = true;
            this.lblmaxhrout.Location = new System.Drawing.Point(3, 3);
            this.lblmaxhrout.Name = "lblmaxhrout";
            this.lblmaxhrout.Size = new System.Drawing.Size(0, 13);
            this.lblmaxhrout.TabIndex = 18;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.lblminhrout);
            this.panel3.Location = new System.Drawing.Point(3, 26);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(110, 21);
            this.panel3.TabIndex = 33;
            // 
            // lblminhrout
            // 
            this.lblminhrout.AutoSize = true;
            this.lblminhrout.Location = new System.Drawing.Point(3, 3);
            this.lblminhrout.Name = "lblminhrout";
            this.lblminhrout.Size = new System.Drawing.Size(0, 13);
            this.lblminhrout.TabIndex = 20;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.lblmaxspeedout);
            this.panel9.Location = new System.Drawing.Point(119, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(110, 21);
            this.panel9.TabIndex = 36;
            // 
            // lblmaxspeedout
            // 
            this.lblmaxspeedout.AutoSize = true;
            this.lblmaxspeedout.Location = new System.Drawing.Point(3, 3);
            this.lblmaxspeedout.Name = "lblmaxspeedout";
            this.lblmaxspeedout.Size = new System.Drawing.Size(0, 13);
            this.lblmaxspeedout.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Maroon;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(110, 21);
            this.panel2.TabIndex = 32;
            // 
            // chk_power
            // 
            this.chk_power.AutoSize = true;
            this.chk_power.Location = new System.Drawing.Point(12, 19);
            this.chk_power.Name = "chk_power";
            this.chk_power.Size = new System.Drawing.Size(93, 17);
            this.chk_power.TabIndex = 3;
            this.chk_power.Text = "Power (Watts)";
            this.chk_power.UseVisualStyleBackColor = true;
            this.chk_power.CheckedChanged += new System.EventHandler(this.chk_power_CheckedChanged);
            // 
            // chk_altitude
            // 
            this.chk_altitude.AutoSize = true;
            this.chk_altitude.Location = new System.Drawing.Point(12, 111);
            this.chk_altitude.Name = "chk_altitude";
            this.chk_altitude.Size = new System.Drawing.Size(78, 17);
            this.chk_altitude.TabIndex = 7;
            this.chk_altitude.Text = "Altitude (m)";
            this.chk_altitude.UseVisualStyleBackColor = true;
            this.chk_altitude.CheckedChanged += new System.EventHandler(this.chk_altitude_CheckedChanged);
            // 
            // chk_HR
            // 
            this.chk_HR.AutoSize = true;
            this.chk_HR.Location = new System.Drawing.Point(12, 42);
            this.chk_HR.Name = "chk_HR";
            this.chk_HR.Size = new System.Drawing.Size(107, 17);
            this.chk_HR.TabIndex = 4;
            this.chk_HR.Text = "HeartRate (BPM)";
            this.chk_HR.UseVisualStyleBackColor = true;
            this.chk_HR.CheckedChanged += new System.EventHandler(this.chk_HR_CheckedChanged);
            // 
            // chk_cadence
            // 
            this.chk_cadence.AutoSize = true;
            this.chk_cadence.Location = new System.Drawing.Point(12, 88);
            this.chk_cadence.Name = "chk_cadence";
            this.chk_cadence.Size = new System.Drawing.Size(102, 17);
            this.chk_cadence.TabIndex = 6;
            this.chk_cadence.Text = "Cadence (RPM)";
            this.chk_cadence.UseVisualStyleBackColor = true;
            this.chk_cadence.CheckedChanged += new System.EventHandler(this.chk_cadence_CheckedChanged);
            // 
            // chk_Speed
            // 
            this.chk_Speed.AutoSize = true;
            this.chk_Speed.Location = new System.Drawing.Point(12, 65);
            this.chk_Speed.Name = "chk_Speed";
            this.chk_Speed.Size = new System.Drawing.Size(94, 17);
            this.chk_Speed.TabIndex = 5;
            this.chk_Speed.Text = "Speed (Km/H)";
            this.chk_Speed.UseVisualStyleBackColor = true;
            this.chk_Speed.CheckedChanged += new System.EventHandler(this.chk_Speed_CheckedChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel18);
            this.tabPage1.Controls.Add(this.monthCalendar1);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1122, 681);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Calendar View";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.label6);
            this.panel18.Controls.Add(this.btn_open_assoc_file);
            this.panel18.Controls.Add(this.lbl_output_assoc_file);
            this.panel18.Location = new System.Drawing.Point(114, 472);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(768, 146);
            this.panel18.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 29);
            this.label6.TabIndex = 2;
            // 
            // btn_open_assoc_file
            // 
            this.btn_open_assoc_file.Location = new System.Drawing.Point(681, 108);
            this.btn_open_assoc_file.Name = "btn_open_assoc_file";
            this.btn_open_assoc_file.Size = new System.Drawing.Size(75, 23);
            this.btn_open_assoc_file.TabIndex = 1;
            this.btn_open_assoc_file.Text = "Open";
            this.btn_open_assoc_file.UseVisualStyleBackColor = true;
            this.btn_open_assoc_file.Click += new System.EventHandler(this.btn_open_assoc_file_Click);
            // 
            // lbl_output_assoc_file
            // 
            this.lbl_output_assoc_file.AutoSize = true;
            this.lbl_output_assoc_file.Location = new System.Drawing.Point(16, 113);
            this.lbl_output_assoc_file.Name = "lbl_output_assoc_file";
            this.lbl_output_assoc_file.Size = new System.Drawing.Size(0, 13);
            this.lbl_output_assoc_file.TabIndex = 0;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.CalendarDimensions = new System.Drawing.Size(4, 3);
            this.monthCalendar1.Location = new System.Drawing.Point(113, 9);
            this.monthCalendar1.MaxSelectionCount = 1;
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 47;
            this.monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(895, 481);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(138, 26);
            this.button6.TabIndex = 46;
            this.button6.Text = "Populate Calendar View";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvints);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1122, 681);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Interval Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvints
            // 
            this.dgvints.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvints.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvints.Location = new System.Drawing.Point(3, 3);
            this.dgvints.Name = "dgvints";
            this.dgvints.Size = new System.Drawing.Size(1116, 675);
            this.dgvints.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 707);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.StatisticsTab.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvints)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage StatisticsTab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblversionout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbltotdistout;
        private System.Windows.Forms.Label lbldatetext;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbltimeout;
        private System.Windows.Forms.Label lbldurationout;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RadioButton rdkm;
        private System.Windows.Forms.RadioButton rdm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblintervalout;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox lblftpin;
        private System.Windows.Forms.Label lbltssout;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblifout;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblnormpowout;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label lblavgaltout;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblmaxaltout;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblavgpowout;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lblavghrout;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblmaxpowout;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label lblavgspeedout;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblmaxhrout;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblminhrout;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lblmaxspeedout;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chk_power;
        private System.Windows.Forms.CheckBox chk_altitude;
        private System.Windows.Forms.CheckBox chk_HR;
        private System.Windows.Forms.CheckBox chk_cadence;
        private System.Windows.Forms.CheckBox chk_Speed;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_open_file;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label notificationlabel;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RichTextBox rtb_int_out;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvints;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_open_assoc_file;
        private System.Windows.Forms.Label lbl_output_assoc_file;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button button6;
    }
}

