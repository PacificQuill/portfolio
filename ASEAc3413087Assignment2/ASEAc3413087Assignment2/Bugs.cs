﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;


namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// The class concerned with reporting bugs, pulling bugs to forms, submitting ammended sections of code and updating bug records accordingly.
    /// </summary>
    public class Bugs
    {

        public MySqlConnection connection;
        public MySqlCommand cmd;
        public MySqlDataReader dataReader;
        public MySqlDataAdapter dataAdapter;

        public int linecount, endid, temppid, tempfid, linestart, lineend;
        public string fixedmessage;
        /// <summary>
        /// Initialises the connection for this class by accepting connection passed from main
        /// </summary>
        /// <param name="passedcon">The connection passed from Main.cs, re-patriated into class-local connection.</param>
        public void BugsConnect(MySqlConnection passedcon) {
            connection = passedcon;
        }
        /// <summary>
        /// Function to populate the grid view at the tob of the bug fix tab.
        /// </summary>
        /// <param name="dgv_fix_bugslist"> grid view on form tab "fix & submit"</param>
        public void populateBugList(DataGridView dgv_fix_bugslist)
        {
            try
            {
                dgv_fix_bugslist.DataSource = null;
                dgv_fix_bugslist.Rows.Clear();
                cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM BUGS WHERE STATUS='Broken'";
                dataAdapter = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dgv_fix_bugslist.AutoResizeColumns();
                dgv_fix_bugslist.DataSource = ds.Tables[0].DefaultView;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Function to populate the Grid View of resolved bugs in the Bug review tab
        /// </summary>
        /// <param name="dgv_review_bugs_list">name of grid view on bug review tab</param>
        public void populateBugReport(DataGridView dgv_review_bugs_list)
        {
            try
            {

                dgv_review_bugs_list.DataSource = null;
                dgv_review_bugs_list.Rows.Clear();
                cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT BUG_ID,MESSAGE,USER FROM BUGS WHERE STATUS ='Resolved'";
                dataAdapter = new MySqlDataAdapter(cmd);
                DataSet ds2 = new DataSet();
                dataAdapter.Fill(ds2);
                dgv_review_bugs_list.AutoResizeColumns();
                dgv_review_bugs_list.DataSource = ds2.Tables[0].DefaultView;
            }
            catch (Exception)
            {
                throw;
            }
        }
    
        /// <summary>
        /// Selects file for which to pull the unresolved bug list combobox entries in the fix and submit tab
        /// </summary>
        /// <param name="cmb_fix_select_file">File selected</param>
        /// <param name="cmb_fix_select_bug">combo box populated by relevant bugs</param>
        public void SelectBuggyFile(ComboBox cmb_fix_select_file, ComboBox cmb_fix_select_bug)
        {
            string query = "SELECT BUG_ID,MESSAGE FROM BUGS WHERE FILE_ID='" + cmb_fix_select_file.SelectedValue + "'";
            dataAdapter = new MySqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "BUGS");
            cmb_fix_select_bug.DisplayMember = "MESSAGE";
            cmb_fix_select_bug.ValueMember = "BUG_ID";
            cmb_fix_select_bug.DataSource = ds.Tables["BUGS"];
        }


       
        /// <summary>
        /// Pushes the data from the submit and fix tab to the database, ammending the file in storage, as well as the bugs individual record
        /// </summary>
        /// <param name="rtb_fix_edit_code">Fixed code from editor richtextbox</param>
        /// <param name="cmb_fix_select_user">User who submitted the fix selected from combobox</param>
        /// <param name="cmb_fix_select_bug">Bug which is relevant selected from combobox</param>
        /// <param name="rtb_fix_msg">User-defined message to update the message held in the bug's record - A solution description </param>
        public void SubmitFix(RichTextBox rtb_fix_edit_code, ComboBox cmb_fix_select_user, ComboBox cmb_fix_select_bug, RichTextBox rtb_fix_msg)
        {
            String[] contents = rtb_fix_edit_code.Lines;
            String[] converted = new String[linecount];

            if (contents.Length + 1 > converted.Length)
            {
                Console.WriteLine("contents bigger than converted");
                for (int i = 0; i < linecount - 1; i++)
                {
                    converted[i] = contents[i];
                }
                for (int i = contents.Length - 1; i >= (converted.Length-1); i--)
                {
                    converted[converted.Length-1] += contents[i];
                }
            }
            else {
                Console.WriteLine("contents bigger than converted");
                for (int i = 0; i < linecount; i++)
                {
                    converted[i] = "";
                }
                for (int i =0;i < contents.Length; i++)
                {
                    converted[i] = contents[i];
                }
                }
            for (int i = 0; i < converted.Length; i++)
            {
                Console.WriteLine(converted[i]);
            }
            int startid = endid - linecount;
            string commandString = "REPLACE INTO FILE_STORAGE(LINE_ID,PROJECT_ID,FILE_ID,AUTHOR_ID,LINE_NO,SUBMITTED,LINE_CONTENT) VALUES (@lid,@pid,@fid,@aid,@lino,@sb,@lc)";
            try
            {
                for (int i = 0; i < linecount-1; i++)
                {
                    Console.WriteLine(commandString);
                    MySqlCommand cmdInsert = new MySqlCommand(commandString, connection);
                    cmdInsert.Parameters.AddWithValue("@lc", converted[i]);
                    cmdInsert.Parameters.AddWithValue("@pid", temppid);
                    cmdInsert.Parameters.AddWithValue("@fid", tempfid);
                    cmdInsert.Parameters.AddWithValue("@aid", cmb_fix_select_user.SelectedValue);
                    cmdInsert.Parameters.AddWithValue("@lino", linestart + i);
                    cmdInsert.Parameters.AddWithValue("@sb", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmdInsert.Parameters.AddWithValue("@lid", startid + 1 + i);
                    cmdInsert.ExecuteNonQuery();
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(" " + ex);
            }
            commandString = "REPLACE INTO BUGS(BUG_ID,STATUS,MESSAGE) VALUES (@bid,@sts,@mes)";

            try
            {
                    fixedmessage = "BUG ON LINES " + linestart.ToString() + " to " + lineend.ToString() + "\n on ";
                    fixedmessage += "FILE NUMBER: " + tempfid + " PROJECT NUMBER: " + temppid + " HAS BEEN RESOLVED.\n\n\n";
                    Console.WriteLine(commandString);
                    MySqlCommand cmdInsert = new MySqlCommand(commandString, connection);
                    cmdInsert.Parameters.AddWithValue("@bid", cmb_fix_select_bug.SelectedValue);
                    cmdInsert.Parameters.AddWithValue("@sts", "Resolved");
                    Console.WriteLine(fixedmessage);
                    cmdInsert.Parameters.AddWithValue("@mes", fixedmessage + rtb_fix_msg.Text);
                    cmdInsert.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(" " + ex);
            }
        }



       /// <summary>
       /// Loads the file/bug pair the user has selected by combobox, and draws the offending code into a datagridview for reference, and a richtextbox for editing
       /// </summary>
       /// <param name="cmb_fix_select_bug">bug user selected</param>
       /// <param name="dgv_fix_orig_code">original code from db</param>
       /// <param name="cmb_fix_select_file">file user selected</param>
       /// <param name="rtb_fix_edit_code">original code loaded to richtextbox for editing</param>
        public void LoadBuggySelection(ComboBox cmb_fix_select_bug, DataGridView dgv_fix_orig_code, ComboBox cmb_fix_select_file, RichTextBox rtb_fix_edit_code)
        {

            string displaystring = "";
            string query = "SELECT LINE_START,LINE_END FROM BUGS WHERE BUG_ID='" + cmb_fix_select_bug.SelectedValue + "'";
            cmd = new MySqlCommand(query, connection);
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                linestart = Int32.Parse(dataReader["LINE_START"].ToString());
                lineend = Int32.Parse(dataReader["LINE_END"].ToString());
            }
            linecount = lineend - linestart + 1;
            dataReader.Close();
            string fixedmessage = "BUG ON LINES " + linestart.ToString() + " to " + lineend.ToString() + "\n on ";
            cmd = new MySqlCommand(query, connection);

            try
            {
                dgv_fix_orig_code.DataSource = null;
                dgv_fix_orig_code.Rows.Clear();
                cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT LINE_CONTENT FROM FILE_STORAGE WHERE LINE_NO BETWEEN '" + linestart + "' AND '" + lineend + "' AND FILE_ID = '" + cmb_fix_select_file.SelectedValue + "'";
                dataAdapter = new MySqlDataAdapter(cmd);
                DataSet ds2 = new DataSet();
                dataAdapter.Fill(ds2);
                dgv_fix_orig_code.AutoGenerateColumns = false;
                dgv_fix_orig_code.ColumnCount = 2;
                dgv_fix_orig_code.Columns[0].Name = "LINE_NO";
                dgv_fix_orig_code.Columns[0].DataPropertyName = "LINE_NO";
                dgv_fix_orig_code.Columns[1].Name = "LINE_CONTENT";
                dgv_fix_orig_code.Columns[1].DataPropertyName = "LINE_CONTENT";
                dgv_fix_orig_code.AutoResizeColumns();
                dgv_fix_orig_code.DataSource = ds2.Tables[0].DefaultView;
                cmd = new MySqlCommand("SELECT LINE_CONTENT,LINE_ID,PROJECT_ID,FILE_ID FROM FILE_STORAGE WHERE LINE_NO BETWEEN '" + linestart + "' AND '" + lineend + "' AND FILE_ID = '" + cmb_fix_select_file.SelectedValue + "'", connection);
                dataReader = cmd.ExecuteReader();
                
                while (dataReader.Read())
                {
                    displaystring += dataReader["LINE_CONTENT"].ToString() + "\n";
                    endid = Int32.Parse(dataReader["LINE_ID"].ToString());
                    temppid = Int32.Parse(dataReader["PROJECT_ID"].ToString());
                    tempfid = Int32.Parse(dataReader["FILE_ID"].ToString());
                }
                displaystring = displaystring.Remove(displaystring.Length - 2);
                rtb_fix_edit_code.Text = displaystring;
                dataReader.Close();

            }
            catch (Exception)
            {
                throw;
            }
           
        }




       /// <summary>
       /// Populates a row of the Bugs table with a new record
       /// </summary>
       /// <param name="cmb_report_select_file">File the bug pertains to</param>
       /// <param name="cmb_report_user">User reporting the bug</param>
       /// <param name="nud_report_line_start">First line of identified bug</param>
       /// <param name="nud_report_line_end">last line of identified bug</param>
       /// <param name="txt_report_class">class of identified bug</param>
       /// <param name="txt_report_method">method of identified bug</param>
       /// <param name="rtb_report_message">Message the user wishes to include, why/how the bug appears, suggestions on how to fix?</param>
        public void CreateBugRecord(ComboBox cmb_report_select_file, ComboBox cmb_report_user, NumericUpDown nud_report_line_start, NumericUpDown nud_report_line_end, TextBox txt_report_class, TextBox txt_report_method, RichTextBox rtb_report_message)
        {
            string thisfilename = "00";
            int thisprojectid = 0;
            string useradding = "BLANK";
            cmd = new MySqlCommand("SELECT FILE_NAME,PROJECT_ID FROM FILES WHERE FILE_ID = " + cmb_report_select_file.SelectedValue + "", connection);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                thisfilename = dataReader["FILE_NAME"].ToString();
                thisprojectid = Int32.Parse(dataReader["PROJECT_ID"].ToString());
            }
            dataReader.Close();
            cmd = new MySqlCommand("SELECT USERNAME FROM USERS WHERE USER_ID = " + cmb_report_user.SelectedValue + "", connection);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                useradding = dataReader["USERNAME"].ToString();

            }
            dataReader.Close();
            string commandString = "INSERT INTO BUGS(LINE_START,LINE_END,FILE_NAME,FILE_ID,CLASS,METHOD,PROJECT,STATUS,MESSAGE,USER,SUBMITTED) VALUES (@ls,@le,@fn,@fi,@c,@m,@p,@s,@me,@us,@dt)";
            try
            {

                Console.WriteLine(commandString);
                cmd = new MySqlCommand(commandString, connection);
                cmd.Parameters.AddWithValue("@ls", Int32.Parse(nud_report_line_start.Text));
                cmd.Parameters.AddWithValue("@le", Int32.Parse(nud_report_line_end.Text));
                cmd.Parameters.AddWithValue("@fn", thisfilename);
                cmd.Parameters.AddWithValue("@c", txt_report_class.Text);
                cmd.Parameters.AddWithValue("@m", txt_report_method.Text);
                cmd.Parameters.AddWithValue("@p", thisprojectid);
                cmd.Parameters.AddWithValue("@s", "Broken");
                cmd.Parameters.AddWithValue("@me", rtb_report_message.Text);
                cmd.Parameters.AddWithValue("@us", useradding);
                cmd.Parameters.AddWithValue("@dt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("@fi", cmb_report_select_file.SelectedValue);

                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(" " + ex);
            }
        }


    }
}
