﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using System.Drawing.Text;
using ASEAc3413087Assignment2;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;


namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// The Main.cs class is used primarily to synchronise the transfer of information from the UI to the appropriate classes
    /// </summary>
    public partial class Main : Form
    {
        public String currentusername;
        public int currentuserid;
        MySqlConnection connection;
        DbConnect databaseGeneralConnection = new DbConnect();
        form_create_user frm;
        Users users = new Users();
        Files files = new Files();
        Projects projects = new Projects();
        Bugs bugs = new Bugs();

        /// <summary>
        /// This section sets up the UI, the connections between each class and the database, as well as calling population of some of the form components from the DB
        /// </summary>
        public Main()
        {
            InitializeComponent();
            connection = databaseGeneralConnection.CreateConnection();
            bugs.BugsConnect(connection);
            users.UsersConnect(connection);
            files.FilesConnect(connection);
            projects.ProjectsConnect(connection);
            users.populateComboBoxes(cmb_upload_user,cmb_report_user,cmb_fix_select_user);
            files.populateFileList(cmb_report_select_file, cmb_fix_select_file);
            files.populateFileGrid(dgv_side_files);
            projects.populateProjectsGrid(dgv_side_projs);
            bugs.populateBugList(dgv_fix_bugslist);
            bugs.populateBugReport(dgv_review_bugs_list);

        }
        /// <summary>
        /// Closes the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
          private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Ties createUser in toolStripMenu to the function which launches the user creation form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            createUser();
        }
        /// <summary>
        /// Ties Welcome tab's LinkLabel to the function that launches user creation form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnk_create_user_Clicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            createUser();
        }
        /// <summary>
        /// Launches user creation form
        /// </summary>
        public void createUser()
        {
            frm = new form_create_user();
            frm.Show();
        }

        /// <summary>
        /// This function populates a 2nd text box (rtb_upload_lineno) with line numbers based on the number of lines in use by the editor pane(rtb_upload_editor)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rtb_upload_editor_KeyDown(object sender, KeyEventArgs e)
        {
            for (int i = 0; i <= rtb_upload_editor.Lines.Count(); i++)
            {
                if (!(e.KeyCode == Keys.Back))
                {
                    if (!rtb_upload_lineno.Text.Contains(i.ToString()))
                    {
                        rtb_upload_lineno.Text += i.ToString() + "\n";
                    }
                }
                else
                {
                    rtb_upload_lineno.Clear();
                }
            }
        }

        /// <summary>
        /// Ties upload tab's load file button to the opendialog function defined in FILES.CS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_upload_loadfile_Click(object sender, EventArgs e)
        {
            files.PullFileFromFS(txt_upload_filename, rtb_upload_editor, rtb_upload_lineno);
        }
        /// <summary>
        /// Ties upload tab's upload button to FILES.CS function to submit the loaded file to the DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_upload_sub_Click(object sender, EventArgs e)
        {
            files.PushToDb(rtb_upload_editor, txt_upload_PID, txt_upload_FID, txt_upload_filename,cmb_upload_user);
            files.populateFileGrid(dgv_side_files);
            files.populateFileList(cmb_report_select_file, cmb_fix_select_file);
        }
        /// <summary>
        /// Ties bug report tab's load file button to function which loads up the file under scrutiny from the DB.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_report_load_file_Click(object sender, EventArgs e)
        {
            files.loadBuggyFile(dgv_report_disp_file, cmb_report_select_file);

        }
        /// <summary>
        /// Ties bug report tab's upload to function in bugs.cs which adds the bug record to the DB.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_report_sub_Click(object sender, EventArgs e)
        {
            bugs.CreateBugRecord(cmb_report_select_file,cmb_report_user,nud_report_line_start,nud_report_line_end,txt_report_class,txt_report_method,rtb_report_message);
            bugs.populateBugList(dgv_fix_bugslist);
            btn_report_sub.Text = "Bug Submitted";
        }
        /// <summary>
        /// Ties Project create tab's only button to the function which will populate the db with a project of the user's submission.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_proj_create_sub_Click(object sender, EventArgs e)
        {
            projects.CreateProject(txt_create_proj_name);
            projects.populateProjectsGrid(dgv_side_projs);
        }
        /// <summary>
        /// Ties bug fix tab's Select button to bugs.cs function which loads related bugs to other textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fix_pull_file_Click(object sender, EventArgs e)
        {
            bugs.SelectBuggyFile(cmb_fix_select_file,cmb_fix_select_bug);
            btn_fix_pull_file.Text = "File Selected";
        }
        /// <summary>
        /// Ties bug fix's Load button to bugs.cs function which pulls section of selected file's code to the editor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fix_load_file_rtb_Click(object sender, EventArgs e)
        {
            bugs.LoadBuggySelection(cmb_fix_select_bug,dgv_fix_orig_code,cmb_fix_select_file, rtb_fix_edit_code);
            btn_fix_load_file_rtb.Text = "Loaded";
        }
        /// <summary>
        /// Ties bug fix's submission button to bugs.cs function which pushes changed code to DB and updates bug record as 'Resolved'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_fix_submit_change_Click(object sender, EventArgs e)
        {
            bugs.SubmitFix(rtb_fix_edit_code,cmb_fix_select_user,cmb_fix_select_bug,rtb_fix_msg);
            bugs.populateBugList(dgv_fix_bugslist);
            bugs.populateBugReport(dgv_review_bugs_list);
        }
    }
}
