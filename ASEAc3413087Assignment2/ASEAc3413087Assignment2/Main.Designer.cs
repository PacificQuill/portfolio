﻿namespace ASEAc3413087Assignment2
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgv_side_projs = new System.Windows.Forms.DataGridView();
            this.dgv_side_files = new System.Windows.Forms.DataGridView();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lnk_create_user = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dis_proj_status = new System.Windows.Forms.Label();
            this.btn_proj_create_sub = new System.Windows.Forms.Button();
            this.txt_create_proj_name = new System.Windows.Forms.TextBox();
            this.proj_create_status = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_upload_sub = new System.Windows.Forms.Button();
            this.txt_upload_filename = new System.Windows.Forms.TextBox();
            this.txt_upload_FID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_upload_PID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmb_upload_user = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_upload_loadfile = new System.Windows.Forms.Button();
            this.rtb_upload_lineno = new System.Windows.Forms.RichTextBox();
            this.rtb_upload_editor = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.nud_report_line_end = new System.Windows.Forms.NumericUpDown();
            this.nud_report_line_start = new System.Windows.Forms.NumericUpDown();
            this.txt_report_class = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmb_report_user = new System.Windows.Forms.ComboBox();
            this.txt_report_method = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_report_sub = new System.Windows.Forms.Button();
            this.rtb_report_message = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgv_report_disp_file = new System.Windows.Forms.DataGridView();
            this.btn_report_load_file = new System.Windows.Forms.Button();
            this.cmb_report_select_file = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.rtb_fix_msg = new System.Windows.Forms.RichTextBox();
            this.btn_fix_submit_change = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.cmb_fix_select_user = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.rtb_fix_edit_code = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dgv_fix_orig_code = new System.Windows.Forms.DataGridView();
            this.btn_fix_load_file_rtb = new System.Windows.Forms.Button();
            this.cmb_fix_select_bug = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btn_fix_pull_file = new System.Windows.Forms.Button();
            this.cmb_fix_select_file = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.dgv_fix_bugslist = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.dgv_review_bugs_list = new System.Windows.Forms.DataGridView();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.undoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllButThisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.menuStrip1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_side_projs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_side_files)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_report_line_end)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_report_line_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_report_disp_file)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_fix_orig_code)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_fix_bugslist)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_review_bugs_list)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1166, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createUserToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // createUserToolStripMenuItem
            // 
            this.createUserToolStripMenuItem.Name = "createUserToolStripMenuItem";
            this.createUserToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.createUserToolStripMenuItem.Text = "Create User...";
            this.createUserToolStripMenuItem.Click += new System.EventHandler(this.createUserToolStripMenuItem_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.dgv_side_projs);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.dgv_side_files);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.monthCalendar1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tabControl1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1166, 741);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 24);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1166, 788);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1166, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel1.Text = "0";
            // 
            // dgv_side_projs
            // 
            this.dgv_side_projs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_side_projs.Location = new System.Drawing.Point(935, 359);
            this.dgv_side_projs.Name = "dgv_side_projs";
            this.dgv_side_projs.RowHeadersVisible = false;
            this.dgv_side_projs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_side_projs.Size = new System.Drawing.Size(201, 150);
            this.dgv_side_projs.TabIndex = 11;
            // 
            // dgv_side_files
            // 
            this.dgv_side_files.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_side_files.Location = new System.Drawing.Point(935, 203);
            this.dgv_side_files.Name = "dgv_side_files";
            this.dgv_side_files.ReadOnly = true;
            this.dgv_side_files.RowHeadersVisible = false;
            this.dgv_side_files.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_side_files.Size = new System.Drawing.Size(201, 150);
            this.dgv_side_files.TabIndex = 10;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(921, 29);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 9;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(909, 741);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lnk_create_user);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(901, 715);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Welcome";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lnk_create_user
            // 
            this.lnk_create_user.AutoSize = true;
            this.lnk_create_user.Location = new System.Drawing.Point(403, 298);
            this.lnk_create_user.Name = "lnk_create_user";
            this.lnk_create_user.Size = new System.Drawing.Size(106, 13);
            this.lnk_create_user.TabIndex = 1;
            this.lnk_create_user.TabStop = true;
            this.lnk_create_user.Text = "Create User Account";
            this.lnk_create_user.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnk_create_user_Clicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(336, 233);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 91);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Reporting and resolution system\r\n\r\nIf this is your first time using this syst" +
    "em, be sure to click\r\nbelow to create your user account\r\n\r\n\r\n\r\n";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dis_proj_status);
            this.tabPage4.Controls.Add(this.btn_proj_create_sub);
            this.tabPage4.Controls.Add(this.txt_create_proj_name);
            this.tabPage4.Controls.Add(this.proj_create_status);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(901, 715);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Create Project";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dis_proj_status
            // 
            this.dis_proj_status.AutoSize = true;
            this.dis_proj_status.Location = new System.Drawing.Point(344, 89);
            this.dis_proj_status.Name = "dis_proj_status";
            this.dis_proj_status.Size = new System.Drawing.Size(0, 13);
            this.dis_proj_status.TabIndex = 5;
            // 
            // btn_proj_create_sub
            // 
            this.btn_proj_create_sub.Location = new System.Drawing.Point(363, 353);
            this.btn_proj_create_sub.Name = "btn_proj_create_sub";
            this.btn_proj_create_sub.Size = new System.Drawing.Size(311, 29);
            this.btn_proj_create_sub.TabIndex = 4;
            this.btn_proj_create_sub.Text = "Create";
            this.btn_proj_create_sub.UseVisualStyleBackColor = true;
            this.btn_proj_create_sub.Click += new System.EventHandler(this.btn_proj_create_sub_Click);
            // 
            // txt_create_proj_name
            // 
            this.txt_create_proj_name.Location = new System.Drawing.Point(443, 311);
            this.txt_create_proj_name.Name = "txt_create_proj_name";
            this.txt_create_proj_name.Size = new System.Drawing.Size(231, 20);
            this.txt_create_proj_name.TabIndex = 3;
            // 
            // proj_create_status
            // 
            this.proj_create_status.AutoSize = true;
            this.proj_create_status.Location = new System.Drawing.Point(17, 89);
            this.proj_create_status.Name = "proj_create_status";
            this.proj_create_status.Size = new System.Drawing.Size(0, 13);
            this.proj_create_status.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(360, 314);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Project Name: ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(360, 275);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Create a project here;";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_upload_sub);
            this.tabPage2.Controls.Add(this.txt_upload_filename);
            this.tabPage2.Controls.Add(this.txt_upload_FID);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txt_upload_PID);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.cmb_upload_user);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.btn_upload_loadfile);
            this.tabPage2.Controls.Add(this.rtb_upload_lineno);
            this.tabPage2.Controls.Add(this.rtb_upload_editor);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(901, 715);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Upload Code";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_upload_sub
            // 
            this.btn_upload_sub.Location = new System.Drawing.Point(376, 652);
            this.btn_upload_sub.Name = "btn_upload_sub";
            this.btn_upload_sub.Size = new System.Drawing.Size(171, 23);
            this.btn_upload_sub.TabIndex = 14;
            this.btn_upload_sub.Text = "Commit file upload to server";
            this.btn_upload_sub.UseVisualStyleBackColor = true;
            this.btn_upload_sub.Click += new System.EventHandler(this.btn_upload_sub_Click);
            // 
            // txt_upload_filename
            // 
            this.txt_upload_filename.Location = new System.Drawing.Point(327, 614);
            this.txt_upload_filename.Name = "txt_upload_filename";
            this.txt_upload_filename.Size = new System.Drawing.Size(100, 20);
            this.txt_upload_filename.TabIndex = 13;
            // 
            // txt_upload_FID
            // 
            this.txt_upload_FID.Location = new System.Drawing.Point(746, 614);
            this.txt_upload_FID.Name = "txt_upload_FID";
            this.txt_upload_FID.Size = new System.Drawing.Size(100, 20);
            this.txt_upload_FID.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(669, 617);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Assign File id:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(264, 617);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "File Name:";
            // 
            // txt_upload_PID
            // 
            this.txt_upload_PID.Location = new System.Drawing.Point(553, 614);
            this.txt_upload_PID.Name = "txt_upload_PID";
            this.txt_upload_PID.Size = new System.Drawing.Size(100, 20);
            this.txt_upload_PID.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(433, 617);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Project_ID (If Known) :";
            // 
            // cmb_upload_user
            // 
            this.cmb_upload_user.FormattingEnabled = true;
            this.cmb_upload_user.Location = new System.Drawing.Point(126, 614);
            this.cmb_upload_user.Name = "cmb_upload_user";
            this.cmb_upload_user.Size = new System.Drawing.Size(132, 21);
            this.cmb_upload_user.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 617);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Submitted by :";
            // 
            // btn_upload_loadfile
            // 
            this.btn_upload_loadfile.Location = new System.Drawing.Point(376, 585);
            this.btn_upload_loadfile.Name = "btn_upload_loadfile";
            this.btn_upload_loadfile.Size = new System.Drawing.Size(171, 23);
            this.btn_upload_loadfile.TabIndex = 2;
            this.btn_upload_loadfile.Text = "Load a file for upload to server";
            this.btn_upload_loadfile.UseVisualStyleBackColor = true;
            this.btn_upload_loadfile.Click += new System.EventHandler(this.btn_upload_loadfile_Click);
            // 
            // rtb_upload_lineno
            // 
            this.rtb_upload_lineno.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.rtb_upload_lineno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtb_upload_lineno.Location = new System.Drawing.Point(6, 6);
            this.rtb_upload_lineno.Name = "rtb_upload_lineno";
            this.rtb_upload_lineno.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtb_upload_lineno.Size = new System.Drawing.Size(51, 574);
            this.rtb_upload_lineno.TabIndex = 1;
            this.rtb_upload_lineno.Text = "";
            // 
            // rtb_upload_editor
            // 
            this.rtb_upload_editor.Location = new System.Drawing.Point(56, 6);
            this.rtb_upload_editor.Name = "rtb_upload_editor";
            this.rtb_upload_editor.Size = new System.Drawing.Size(821, 574);
            this.rtb_upload_editor.TabIndex = 0;
            this.rtb_upload_editor.Text = "";
            this.rtb_upload_editor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtb_upload_editor_KeyDown);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.nud_report_line_end);
            this.tabPage3.Controls.Add(this.nud_report_line_start);
            this.tabPage3.Controls.Add(this.txt_report_class);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.cmb_report_user);
            this.tabPage3.Controls.Add(this.txt_report_method);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.btn_report_sub);
            this.tabPage3.Controls.Add(this.rtb_report_message);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.dgv_report_disp_file);
            this.tabPage3.Controls.Add(this.btn_report_load_file);
            this.tabPage3.Controls.Add(this.cmb_report_select_file);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(901, 715);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Report Bug";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // nud_report_line_end
            // 
            this.nud_report_line_end.Location = new System.Drawing.Point(127, 644);
            this.nud_report_line_end.Name = "nud_report_line_end";
            this.nud_report_line_end.Size = new System.Drawing.Size(55, 20);
            this.nud_report_line_end.TabIndex = 19;
            // 
            // nud_report_line_start
            // 
            this.nud_report_line_start.Location = new System.Drawing.Point(127, 619);
            this.nud_report_line_start.Name = "nud_report_line_start";
            this.nud_report_line_start.Size = new System.Drawing.Size(55, 20);
            this.nud_report_line_start.TabIndex = 18;
            // 
            // txt_report_class
            // 
            this.txt_report_class.Location = new System.Drawing.Point(323, 671);
            this.txt_report_class.Name = "txt_report_class";
            this.txt_report_class.Size = new System.Drawing.Size(56, 20);
            this.txt_report_class.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(189, 674);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 13);
            this.label15.TabIndex = 15;
            this.label15.Text = "Class containing bug:";
            // 
            // cmb_report_user
            // 
            this.cmb_report_user.FormattingEnabled = true;
            this.cmb_report_user.Location = new System.Drawing.Point(323, 617);
            this.cmb_report_user.Name = "cmb_report_user";
            this.cmb_report_user.Size = new System.Drawing.Size(121, 21);
            this.cmb_report_user.TabIndex = 14;
            // 
            // txt_report_method
            // 
            this.txt_report_method.Location = new System.Drawing.Point(323, 645);
            this.txt_report_method.Name = "txt_report_method";
            this.txt_report_method.Size = new System.Drawing.Size(56, 20);
            this.txt_report_method.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(189, 651);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Method containing bug:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(189, 621);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Bug Report Submitted by:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 651);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Line of bug (end) :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 621);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Line of bug (Start) :";
            // 
            // btn_report_sub
            // 
            this.btn_report_sub.Location = new System.Drawing.Point(732, 644);
            this.btn_report_sub.Name = "btn_report_sub";
            this.btn_report_sub.Size = new System.Drawing.Size(142, 20);
            this.btn_report_sub.TabIndex = 6;
            this.btn_report_sub.Text = "Submit to Database.";
            this.btn_report_sub.UseVisualStyleBackColor = true;
            this.btn_report_sub.Click += new System.EventHandler(this.btn_report_sub_Click);
            // 
            // rtb_report_message
            // 
            this.rtb_report_message.Location = new System.Drawing.Point(24, 524);
            this.rtb_report_message.Name = "rtb_report_message";
            this.rtb_report_message.Size = new System.Drawing.Size(850, 90);
            this.rtb_report_message.TabIndex = 5;
            this.rtb_report_message.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 507);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(159, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Please Describe the Bug Below:";
            // 
            // dgv_report_disp_file
            // 
            this.dgv_report_disp_file.AllowUserToAddRows = false;
            this.dgv_report_disp_file.AllowUserToDeleteRows = false;
            this.dgv_report_disp_file.AllowUserToResizeColumns = false;
            this.dgv_report_disp_file.AllowUserToResizeRows = false;
            this.dgv_report_disp_file.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_report_disp_file.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_report_disp_file.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_report_disp_file.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_report_disp_file.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_report_disp_file.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_report_disp_file.Location = new System.Drawing.Point(24, 82);
            this.dgv_report_disp_file.Name = "dgv_report_disp_file";
            this.dgv_report_disp_file.ReadOnly = true;
            this.dgv_report_disp_file.Size = new System.Drawing.Size(850, 418);
            this.dgv_report_disp_file.TabIndex = 3;
            // 
            // btn_report_load_file
            // 
            this.btn_report_load_file.Location = new System.Drawing.Point(336, 51);
            this.btn_report_load_file.Name = "btn_report_load_file";
            this.btn_report_load_file.Size = new System.Drawing.Size(206, 23);
            this.btn_report_load_file.TabIndex = 2;
            this.btn_report_load_file.Text = "Load file from DB";
            this.btn_report_load_file.UseVisualStyleBackColor = true;
            this.btn_report_load_file.Click += new System.EventHandler(this.btn_report_load_file_Click);
            // 
            // cmb_report_select_file
            // 
            this.cmb_report_select_file.FormattingEnabled = true;
            this.cmb_report_select_file.Location = new System.Drawing.Point(90, 24);
            this.cmb_report_select_file.Name = "cmb_report_select_file";
            this.cmb_report_select_file.Size = new System.Drawing.Size(746, 21);
            this.cmb_report_select_file.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(389, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Please select the file from the drop down which you would like to report a bug fo" +
    "r:";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.rtb_fix_msg);
            this.tabPage6.Controls.Add(this.btn_fix_submit_change);
            this.tabPage6.Controls.Add(this.label23);
            this.tabPage6.Controls.Add(this.cmb_fix_select_user);
            this.tabPage6.Controls.Add(this.label22);
            this.tabPage6.Controls.Add(this.rtb_fix_edit_code);
            this.tabPage6.Controls.Add(this.label21);
            this.tabPage6.Controls.Add(this.dgv_fix_orig_code);
            this.tabPage6.Controls.Add(this.btn_fix_load_file_rtb);
            this.tabPage6.Controls.Add(this.cmb_fix_select_bug);
            this.tabPage6.Controls.Add(this.label20);
            this.tabPage6.Controls.Add(this.btn_fix_pull_file);
            this.tabPage6.Controls.Add(this.cmb_fix_select_file);
            this.tabPage6.Controls.Add(this.label19);
            this.tabPage6.Controls.Add(this.dgv_fix_bugslist);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(901, 715);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Select & Fix";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // rtb_fix_msg
            // 
            this.rtb_fix_msg.Location = new System.Drawing.Point(24, 618);
            this.rtb_fix_msg.Name = "rtb_fix_msg";
            this.rtb_fix_msg.Size = new System.Drawing.Size(531, 62);
            this.rtb_fix_msg.TabIndex = 14;
            this.rtb_fix_msg.Text = "-Please enter any message here";
            // 
            // btn_fix_submit_change
            // 
            this.btn_fix_submit_change.Location = new System.Drawing.Point(574, 656);
            this.btn_fix_submit_change.Name = "btn_fix_submit_change";
            this.btn_fix_submit_change.Size = new System.Drawing.Size(307, 24);
            this.btn_fix_submit_change.TabIndex = 13;
            this.btn_fix_submit_change.Text = "Submit Fix";
            this.btn_fix_submit_change.UseVisualStyleBackColor = true;
            this.btn_fix_submit_change.Click += new System.EventHandler(this.btn_fix_submit_change_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(571, 624);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "Fixed By:";
            // 
            // cmb_fix_select_user
            // 
            this.cmb_fix_select_user.FormattingEnabled = true;
            this.cmb_fix_select_user.Location = new System.Drawing.Point(760, 624);
            this.cmb_fix_select_user.Name = "cmb_fix_select_user";
            this.cmb_fix_select_user.Size = new System.Drawing.Size(121, 21);
            this.cmb_fix_select_user.TabIndex = 11;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(21, 461);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(128, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Include fixed code below:";
            // 
            // rtb_fix_edit_code
            // 
            this.rtb_fix_edit_code.Location = new System.Drawing.Point(21, 480);
            this.rtb_fix_edit_code.Name = "rtb_fix_edit_code";
            this.rtb_fix_edit_code.Size = new System.Drawing.Size(849, 132);
            this.rtb_fix_edit_code.TabIndex = 9;
            this.rtb_fix_edit_code.Text = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 407);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(469, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Original, Buggy code, will be shown in the box above, please reference this to as" +
    "certain how to fix.";
            // 
            // dgv_fix_orig_code
            // 
            this.dgv_fix_orig_code.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_fix_orig_code.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_fix_orig_code.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_fix_orig_code.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv_fix_orig_code.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_fix_orig_code.Location = new System.Drawing.Point(21, 249);
            this.dgv_fix_orig_code.Name = "dgv_fix_orig_code";
            this.dgv_fix_orig_code.ReadOnly = true;
            this.dgv_fix_orig_code.Size = new System.Drawing.Size(860, 143);
            this.dgv_fix_orig_code.TabIndex = 7;
            // 
            // btn_fix_load_file_rtb
            // 
            this.btn_fix_load_file_rtb.Location = new System.Drawing.Point(776, 206);
            this.btn_fix_load_file_rtb.Name = "btn_fix_load_file_rtb";
            this.btn_fix_load_file_rtb.Size = new System.Drawing.Size(105, 23);
            this.btn_fix_load_file_rtb.TabIndex = 6;
            this.btn_fix_load_file_rtb.Text = "Load";
            this.btn_fix_load_file_rtb.UseVisualStyleBackColor = true;
            this.btn_fix_load_file_rtb.Click += new System.EventHandler(this.btn_fix_load_file_rtb_Click);
            // 
            // cmb_fix_select_bug
            // 
            this.cmb_fix_select_bug.FormattingEnabled = true;
            this.cmb_fix_select_bug.Location = new System.Drawing.Point(171, 208);
            this.cmb_fix_select_bug.Name = "cmb_fix_select_bug";
            this.cmb_fix_select_bug.Size = new System.Drawing.Size(599, 21);
            this.cmb_fix_select_bug.TabIndex = 5;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(18, 211);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(106, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Select Bug to tackle:";
            // 
            // btn_fix_pull_file
            // 
            this.btn_fix_pull_file.Location = new System.Drawing.Point(776, 176);
            this.btn_fix_pull_file.Name = "btn_fix_pull_file";
            this.btn_fix_pull_file.Size = new System.Drawing.Size(105, 23);
            this.btn_fix_pull_file.TabIndex = 3;
            this.btn_fix_pull_file.Text = "Select";
            this.btn_fix_pull_file.UseVisualStyleBackColor = true;
            this.btn_fix_pull_file.Click += new System.EventHandler(this.btn_fix_pull_file_Click);
            // 
            // cmb_fix_select_file
            // 
            this.cmb_fix_select_file.FormattingEnabled = true;
            this.cmb_fix_select_file.Location = new System.Drawing.Point(171, 178);
            this.cmb_fix_select_file.Name = "cmb_fix_select_file";
            this.cmb_fix_select_file.Size = new System.Drawing.Size(599, 21);
            this.cmb_fix_select_file.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(18, 181);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(147, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Select Project / File to tackle:";
            // 
            // dgv_fix_bugslist
            // 
            this.dgv_fix_bugslist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_fix_bugslist.Location = new System.Drawing.Point(18, 19);
            this.dgv_fix_bugslist.Name = "dgv_fix_bugslist";
            this.dgv_fix_bugslist.Size = new System.Drawing.Size(863, 137);
            this.dgv_fix_bugslist.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.dgv_review_bugs_list);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(901, 715);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Bug Review";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(39, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(292, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "From here you can view all of the Bug Resolution comments:";
            // 
            // dgv_review_bugs_list
            // 
            this.dgv_review_bugs_list.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_review_bugs_list.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_review_bugs_list.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_review_bugs_list.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgv_review_bugs_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_review_bugs_list.Location = new System.Drawing.Point(23, 62);
            this.dgv_review_bugs_list.Name = "dgv_review_bugs_list";
            this.dgv_review_bugs_list.ReadOnly = true;
            this.dgv_review_bugs_list.Size = new System.Drawing.Size(849, 633);
            this.dgv_review_bugs_list.TabIndex = 0;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "txt";
            this.saveFileDialog1.Filter = "Text Files|*.txt|VB Files|*.vb|C# Files|*.cs|All Files|*.*";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Text Files|*.txt|VB Files|*.vb|C# Files|*.cs|All Files|*.*";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem1,
            this.redoToolStripMenuItem1,
            this.toolStripSeparator12,
            this.cutToolStripMenuItem1,
            this.copyToolStripMenuItem1,
            this.pasteToolStripMenuItem1,
            this.toolStripSeparator13,
            this.saveToolStripMenuItem1,
            this.closeToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.closeAllButThisToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(145, 214);
            // 
            // undoToolStripMenuItem1
            // 
            this.undoToolStripMenuItem1.Name = "undoToolStripMenuItem1";
            this.undoToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem1.Text = "Undo";
            // 
            // redoToolStripMenuItem1
            // 
            this.redoToolStripMenuItem1.Name = "redoToolStripMenuItem1";
            this.redoToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem1.Text = "Redo";
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(141, 6);
            // 
            // cutToolStripMenuItem1
            // 
            this.cutToolStripMenuItem1.Name = "cutToolStripMenuItem1";
            this.cutToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.cutToolStripMenuItem1.Text = "Cut";
            // 
            // copyToolStripMenuItem1
            // 
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            this.copyToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem1.Text = "Copy";
            // 
            // pasteToolStripMenuItem1
            // 
            this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
            this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.pasteToolStripMenuItem1.Text = "Paste";
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(141, 6);
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.saveToolStripMenuItem1.Text = "Save";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.closeAllToolStripMenuItem.Text = "Close All";
            // 
            // closeAllButThisToolStripMenuItem
            // 
            this.closeAllButThisToolStripMenuItem.Name = "closeAllButThisToolStripMenuItem";
            this.closeAllButThisToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.closeAllButThisToolStripMenuItem.Text = "Close All But This";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1166, 812);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Home & Text Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_side_projs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_side_files)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_report_line_end)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_report_line_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_report_disp_file)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_fix_orig_code)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_fix_bugslist)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_review_bugs_list)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllButThisToolStripMenuItem;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.LinkLabel lnk_create_user;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox rtb_upload_editor;
        private System.Windows.Forms.RichTextBox rtb_upload_lineno;
        private System.Windows.Forms.Button btn_upload_loadfile;
        private System.Windows.Forms.ComboBox cmb_upload_user;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.TextBox txt_upload_PID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_upload_FID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_upload_filename;
        private System.Windows.Forms.DataGridView dgv_side_projs;
        private System.Windows.Forms.DataGridView dgv_side_files;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btn_proj_create_sub;
        private System.Windows.Forms.TextBox txt_create_proj_name;
        private System.Windows.Forms.Label proj_create_status;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label dis_proj_status;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView dgv_review_bugs_list;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dgv_fix_bugslist;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.NumericUpDown nud_report_line_end;
        private System.Windows.Forms.NumericUpDown nud_report_line_start;
        private System.Windows.Forms.TextBox txt_report_class;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmb_report_user;
        private System.Windows.Forms.TextBox txt_report_method;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_report_sub;
        private System.Windows.Forms.RichTextBox rtb_report_message;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgv_report_disp_file;
        private System.Windows.Forms.Button btn_report_load_file;
        private System.Windows.Forms.ComboBox cmb_report_select_file;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_fix_pull_file;
        private System.Windows.Forms.ComboBox cmb_fix_select_file;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btn_fix_load_file_rtb;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView dgv_fix_orig_code;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.RichTextBox rtb_fix_edit_code;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmb_fix_select_user;
        private System.Windows.Forms.Button btn_fix_submit_change;
        private System.Windows.Forms.RichTextBox rtb_fix_msg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_upload_sub;
        private System.Windows.Forms.ComboBox cmb_fix_select_bug;
    }
}

