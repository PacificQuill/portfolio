﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;



namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// Class to handle how files and meta are taken from and submitted to the database
    /// </summary>
    public class Files
    {

        public MySqlConnection connection;
        /// <summary>
        /// Recieves connection from Main.cs into local connection
        /// </summary>
        /// <param name="passedcon">The connection from Main.Cs</param>
        public void FilesConnect(MySqlConnection passedcon)
        {
            connection = passedcon;
        }

         /// <summary>
         /// This populates the file list comboboxes, one on the report bug tab, one on the bug fix tab; allows user to select a file
         /// </summary>
         /// <param name="cmb_report_select_file">combo box on bug report tab</param>
         /// <param name="cmb_fix_select_file">combo box on bug fix tab</param>
        public void populateFileList(ComboBox cmb_report_select_file, ComboBox cmb_fix_select_file)
        {
            string query = "SELECT FILE_ID,FILE_NAME,PROJECT_ID,FILE_CONCAT FROM FILES";
            MySqlDataAdapter da = new MySqlDataAdapter(query, connection);
            DataSet ds2 = new DataSet();
            da.Fill(ds2, "FILES");
            cmb_report_select_file.DisplayMember = "FILE_CONCAT";
            cmb_fix_select_file.DisplayMember = "FILE_CONCAT";
            cmb_report_select_file.ValueMember = "FILE_ID";
            cmb_fix_select_file.ValueMember = "FILE_ID";
            cmb_report_select_file.DataSource = ds2.Tables["FILES"];
            cmb_fix_select_file.DataSource = ds2.Tables["FILES"];
        }


        /// <summary>
        /// Populates the datagridview of files at the side of the UI.
        /// </summary>
        /// <param name="dgv_side_files">The name of the side bar dataGridView</param>
        public void populateFileGrid(DataGridView dgv_side_files)
        {
            try
            {
                dgv_side_files.DataSource = null;
                dgv_side_files.Rows.Clear();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * from FILES";
                MySqlDataAdapter adap = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                dgv_side_files.AutoGenerateColumns = false;
                dgv_side_files.ColumnCount = 2;
                dgv_side_files.Columns[0].Name = "FILE_ID";
                dgv_side_files.Columns[0].DataPropertyName = "FILE_ID";
                dgv_side_files.Columns[1].Name = "FILE_NAME";
                dgv_side_files.Columns[1].DataPropertyName = "FILE_NAME";
                dgv_side_files.AutoResizeColumns();
                dgv_side_files.DataSource = ds.Tables[0].DefaultView;


            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// This function handles the loading of a file from the users filesystem onto the application
        /// </summary>
        /// <param name="txt_upload_filename">name of the file(populated by openfiledialog), can be reset using textbox; when submitted to db</param>
        /// <param name="rtb_upload_editor">name of the editor pane that the file is loaded into before submission</param>
        /// <param name="rtb_upload_lineno">name of the line number rich text box running down the side of the editor</param>
        public void PullFileFromFS(TextBox txt_upload_filename, RichTextBox rtb_upload_editor, RichTextBox rtb_upload_lineno)
        {

            //Section to Select & Pull
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "All Files|*.*|RTF|*.rtf|Text Files|*.txt|VB Files|*.vb|C# Files|*.cs";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt_upload_filename.Text = ofd.SafeFileName;
                string FileToOpen = ofd.FileName;
                System.IO.StreamReader reader = new System.IO.StreamReader(FileToOpen);
                string[] values = (reader.ReadToEnd()).Split('\n');
                rtb_upload_lineno.Clear();
                rtb_upload_editor.Clear();
                for (int i = 0; i < values.Length; i++)
                {
                    rtb_upload_editor.Text += values[i];
                    if (!rtb_upload_lineno.Text.Contains(i.ToString()))
                    {
                        rtb_upload_lineno.Text += i.ToString() + "\n";
                    }
                }
            }
        }

        /// <summary>
        /// handles submission of the file, loaded into the editor text box, to the database
        /// </summary>
        /// <param name="rtb_upload_editor">editor text box</param>
        /// <param name="txt_upload_PID">project to link file to:user-defined</param>
        /// <param name="txt_upload_FID">file id of the file:user-defined</param>
        /// <param name="txt_upload_filename">filename to go on DB, set from openfiledialog but can be changed</param>
        /// <param name="cmb_upload_user">user selected as "owner" from the combobox</param>
        public void PushToDb(RichTextBox rtb_upload_editor, TextBox txt_upload_PID, TextBox txt_upload_FID, TextBox txt_upload_filename, ComboBox cmb_upload_user)
        {

            String[] line_cont = rtb_upload_editor.Lines;
            int projid;
            int fileid;
            if (string.IsNullOrWhiteSpace(txt_upload_PID.Text))
            {
                projid = 0;
            }
            else
            {
                projid = Int32.Parse(txt_upload_PID.Text);
            }
            if (string.IsNullOrWhiteSpace(txt_upload_FID.Text))
            {
                fileid = 0;
            }
            else
            {
                fileid = Int32.Parse(txt_upload_FID.Text);
            }

            string commandString = "INSERT INTO files(FILE_ID,PROJECT_ID,FILE_NAME,FILE_CONCAT) VALUES (@fileid,@projid,@filename,@concat)";
            try
            {

                Console.WriteLine(commandString);
                MySqlCommand cmdInsert = new MySqlCommand(commandString, connection);
                cmdInsert.Parameters.AddWithValue("@projid", projid);
                cmdInsert.Parameters.AddWithValue("@filename", txt_upload_filename.Text);
                cmdInsert.Parameters.AddWithValue("@fileid", fileid);
                cmdInsert.Parameters.AddWithValue("@concat", txt_upload_filename.Text + "/" + fileid + "/" + projid);
                cmdInsert.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(" " + ex);
            }
            int x = 0;
            for (int line_no = 0; line_no < line_cont.Length; line_no++)
            {
                commandString = "INSERT INTO file_storage(PROJECT_ID,FILE_ID,AUTHOR_ID,LINE_NO,SUBMITTED,LINE_CONTENT) VALUES (@projid,@fileid,@author,@line_no,@submitted,@line_content)";
                try
                {

                    Console.WriteLine(commandString);
                    MySqlCommand cmdInsert = new MySqlCommand(commandString, connection);
                    cmdInsert.Parameters.AddWithValue("@projid", projid);
                    cmdInsert.Parameters.AddWithValue("@fileid", fileid);
                    cmdInsert.Parameters.AddWithValue("@author", (int)cmb_upload_user.SelectedValue);
                    cmdInsert.Parameters.AddWithValue("@line_no", x);
                    cmdInsert.Parameters.AddWithValue("@submitted", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmdInsert.Parameters.AddWithValue("@line_content", line_cont[x]);
                    cmdInsert.ExecuteNonQuery();

                }
                catch (MySqlException ex)
                {
                    MessageBox.Show(" " + ex);
                }
                x++;
            }
        }

        /// <summary>
        /// Loads file from database into the Bug report tab dataGridView, used to scrutinise before submitting bug report
        /// </summary>
        /// <param name="dgv_report_disp_file">datagridview the file is loaded into as Line number-Content pairs.</param>
        /// <param name="cmb_report_select_file">The file they selected, used to identify file to pull</param>
        public void loadBuggyFile(DataGridView dgv_report_disp_file, ComboBox cmb_report_select_file)
        {
            try
            {
                dgv_report_disp_file.DataSource = null;
                dgv_report_disp_file.Rows.Clear();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * from FILE_STORAGE where FILE_ID =" + cmb_report_select_file.SelectedValue;
                MySqlDataAdapter adap = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                dgv_report_disp_file.AutoGenerateColumns = false;
                dgv_report_disp_file.ColumnCount = 2;
                dgv_report_disp_file.Columns[0].Name = "LINE_NO";
                dgv_report_disp_file.Columns[0].DataPropertyName = "LINE_NO";
                dgv_report_disp_file.Columns[1].Name = "LINE_CONTENT";
                dgv_report_disp_file.Columns[1].DataPropertyName = "LINE_CONTENT";
                dgv_report_disp_file.AutoResizeColumns();
                dgv_report_disp_file.DataSource = ds.Tables[0].DefaultView;

            }
            catch (Exception)
            {
                throw;
            }
        }



    }
}
