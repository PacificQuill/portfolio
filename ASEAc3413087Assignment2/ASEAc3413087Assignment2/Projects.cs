﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;


namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// This Class is for handling creation of projects and passing of projects records to comboboxes for selection in other tabs
    /// </summary>
    public class Projects
    {

        public MySqlConnection connection;

        /// <summary>
        /// Recieves the connection passed by Main.cs into a local connection to be used throughout
        /// </summary>
        /// <param name="passedcon"></param>
        public void ProjectsConnect(MySqlConnection passedcon)
        {
            connection = passedcon;
        }
        /// <summary>
        /// populated the datagridview in the sidebar of the ui
        /// </summary>
        /// <param name="dgv_side_projs">data grid view name so it can be fed data</param>
        public void populateProjectsGrid(DataGridView dgv_side_projs)
        {
            try
            {
                dgv_side_projs.DataSource = null;
                dgv_side_projs.Rows.Clear();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM PROJECT";
                MySqlDataAdapter adap = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                dgv_side_projs.AutoGenerateColumns = false;
                dgv_side_projs.ColumnCount = 2;
                dgv_side_projs.Columns[0].Name = "PROJECT_ID";
                dgv_side_projs.Columns[0].DataPropertyName = "PROJECT_ID";
                dgv_side_projs.Columns[1].Name = "PROJECT_NAME";
                dgv_side_projs.Columns[1].DataPropertyName = "PROJECT_NAME";
                dgv_side_projs.AutoResizeColumns();
                dgv_side_projs.DataSource = ds.Tables[0].DefaultView;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// handles creation of new project record
        /// </summary>
        /// <param name="txt_create_proj_name">name of project to push to db</param>
        public void CreateProject(TextBox txt_create_proj_name)
        {
            string commandString = "INSERT INTO PROJECT(PROJECT_NAME) VALUES (@Projname)";
            try
            {

                Console.WriteLine(commandString);
                MySqlCommand cmdInsert = new MySqlCommand(commandString, connection);
                cmdInsert.Parameters.AddWithValue("@Projname", txt_create_proj_name.Text);
                cmdInsert.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(" " + ex);
            }
        }

    }
}
