﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// Class pertaining to pulling users
    /// </summary>
    public class Users
    {

        public MySqlConnection connection;
        /// <summary>
        /// Connection is passed from main.cs and bound to local connection
        /// </summary>
        /// <param name="passedcon">passed connection</param>
        public void UsersConnect(MySqlConnection passedcon)
        {
            connection = passedcon;
        }
        /// <summary>
        /// This function populates all the comboboxes in the tabs of the ui which allow the user to select a user (for bug submission / uploading / resolution)
        /// </summary>
        /// <param name="cmb_upload_user">combobox on upload tab to select who is uploading</param>
        /// <param name="cmb_report_user">combobox on bug report tab to select who is report a bug</param>
        /// <param name="cmb_fix_select_user">combobox on bug fix tab to select who is submitting a bug resolution</param>
        public void populateComboBoxes(ComboBox cmb_upload_user, ComboBox cmb_report_user, ComboBox cmb_fix_select_user)
        {
            string query = "SELECT USER_ID,USERNAME FROM USERS";
            MySqlDataAdapter da = new MySqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            da.Fill(ds, "Users");
            cmb_upload_user.DisplayMember = "USERNAME";
            cmb_upload_user.ValueMember = "USER_ID";
            cmb_upload_user.DataSource = ds.Tables["USERS"];
            cmb_upload_user.SelectedIndex = 0;
            cmb_upload_user.DisplayMember = "USERNAME";
            cmb_report_user.ValueMember = "USER_ID";
            cmb_report_user.DataSource = ds.Tables["USERS"];
            cmb_report_user.SelectedIndex = 0;
            cmb_report_user.DisplayMember = "USERNAME";
            cmb_fix_select_user.ValueMember = "USER_ID";
            cmb_fix_select_user.DataSource = ds.Tables["USERS"];
            cmb_fix_select_user.SelectedIndex = 0;
            cmb_fix_select_user.DisplayMember = "USERNAME";
        }
    }
}
