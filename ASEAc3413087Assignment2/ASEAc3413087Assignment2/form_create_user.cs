﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;

namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// This form / Class is just to facilitate the in-application creation of new Users for use of tagging bugs.
    /// </summary>
    public partial class form_create_user : Form
    {
        private MySqlConnection connection;
        private string dbserver;
        private string dbdatabase;
        private string dbuid;
        private string dbpassword;

        //Block of Variables for user Creation
        private String uifirstname;
        private String uilastname;
        private String uiusername;
        private String uipassword;
        private String uipassword2;
        private String uirole;
        private String uihashedpass;

        /// <summary>
        /// Initializes the form and populate the role combobox
        /// </summary>
        public form_create_user()
        {
            InitializeComponent();
            InitializeDBConnection();
            comboBox1.Items.Add("Programmer");
            comboBox1.Items.Add("Tester");
            comboBox1.Items.Add("Developer");
        }

        /// <summary>
        /// Validates all text boxes are filled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_create_user_Click(object sender, EventArgs e)
        {
            uifirstname = tb_cu_Firstname.Text;
            uilastname = tb_cu_Surname.Text;
            uiusername = tb_cu_Username.Text;
            uipassword = tb_cu_Password.Text;
            uipassword2 = tb_cu_Reenter.Text;
            uirole = comboBox1.Text;

            if ((uifirstname != "") && (uilastname != "") && (uiusername != "") && (uipassword != "") && (uipassword2 != "") && (uirole != ""))
            {
                if (uipassword == uipassword2)
                {
                    //Hashes password.
                    using (MD5 md5Hash = MD5.Create())
                    {
                         uihashedpass = GetMd5Hash(md5Hash, uipassword);

                        if (VerifyMd5Hash(md5Hash, uipassword, uihashedpass))
                        {
                            Console.WriteLine("The hashes are the same. Password submitted to database");
                            InsertUserRecord();
                        }
                        else
                        {
                            Console.WriteLine("The hashes are not same. Not sent");
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Please ensure you have entered the same password in both boxes before trying again.");
                }

            }
            else
            {
                MessageBox.Show("One or more of the fields have been left blank, Please check that you have filled them out and try again");
            }

        }

        /// <summary>
        /// Co-ordinates submission of user details to the db
        /// </summary>
        private void InsertUserRecord()
        {
            string commandString = "INSERT INTO users(FIRSTNAME, LASTNAME, USERNAME, PASSWORD, ROLE) VALUES (@uifirstname, @uilastname, @uiusername, @uihashedpass, @uirole)";
            Console.WriteLine(commandString);
            insertRecord(commandString);
        }

        /// <summary>
        /// populates the alias's of the already constructed query to produce a proper SQL command.
        /// </summary>
        /// <param name="commandString">This</param>
        public void insertRecord(String commandString)
        {
            try
            {
                MySqlCommand cmdInsert = new MySqlCommand(commandString, connection);
                cmdInsert.Parameters.AddWithValue("@uifirstname", uifirstname);
                cmdInsert.Parameters.AddWithValue("@uilastname", uilastname);
                cmdInsert.Parameters.AddWithValue("@uiusername", uiusername);
                cmdInsert.Parameters.AddWithValue("@uihashedpass", uihashedpass);
                cmdInsert.Parameters.AddWithValue("@uirole", uirole);
                Console.WriteLine(commandString);
                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("The user " + uiusername + " was created successfully.");
                this.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("The user " + uiusername + " was not created successfully. Please try again"+"/n The error code is: "+ex);
            }

        }


        /// <summary>
        /// Sets up connection to Database
        /// </summary>
        private void InitializeDBConnection()
        {
            dbserver = "localhost";
            dbdatabase = "assig2";
            dbuid = "root";
            dbpassword = "";
            string connectionString;
            connectionString = "SERVER=" + dbserver + ";" + "DATABASE=" +
            dbdatabase + ";" + "UID=" + dbuid + ";" + "PASSWORD=" + dbpassword + ";";
            connection = new MySqlConnection(connectionString);
            connection.Open();
            
        }





        /// <summary>
        /// produces MD5 hash of user's password, returns 
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns>hashed string for submission to db</returns>
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }



        /// <summary>
        /// For verification of returned hash values
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
