﻿namespace ASEAc3413087Assignment2
{
    partial class form_create_user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_cu_Username = new System.Windows.Forms.TextBox();
            this.tb_cu_Password = new System.Windows.Forms.TextBox();
            this.tb_cu_Firstname = new System.Windows.Forms.TextBox();
            this.tb_cu_Surname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_create_user = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tb_cu_Reenter = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_cu_Username
            // 
            this.tb_cu_Username.Location = new System.Drawing.Point(103, 60);
            this.tb_cu_Username.Name = "tb_cu_Username";
            this.tb_cu_Username.Size = new System.Drawing.Size(214, 20);
            this.tb_cu_Username.TabIndex = 0;
            // 
            // tb_cu_Password
            // 
            this.tb_cu_Password.Location = new System.Drawing.Point(103, 86);
            this.tb_cu_Password.Name = "tb_cu_Password";
            this.tb_cu_Password.PasswordChar = '•';
            this.tb_cu_Password.Size = new System.Drawing.Size(214, 20);
            this.tb_cu_Password.TabIndex = 1;
            // 
            // tb_cu_Firstname
            // 
            this.tb_cu_Firstname.Location = new System.Drawing.Point(103, 141);
            this.tb_cu_Firstname.Name = "tb_cu_Firstname";
            this.tb_cu_Firstname.Size = new System.Drawing.Size(214, 20);
            this.tb_cu_Firstname.TabIndex = 3;
            // 
            // tb_cu_Surname
            // 
            this.tb_cu_Surname.Location = new System.Drawing.Point(103, 169);
            this.tb_cu_Surname.Name = "tb_cu_Surname";
            this.tb_cu_Surname.Size = new System.Drawing.Size(214, 20);
            this.tb_cu_Surname.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Username :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "First Name :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Surname :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(42, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(238, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Create User Account:";
            // 
            // btn_create_user
            // 
            this.btn_create_user.Location = new System.Drawing.Point(39, 222);
            this.btn_create_user.Name = "btn_create_user";
            this.btn_create_user.Size = new System.Drawing.Size(278, 27);
            this.btn_create_user.TabIndex = 6;
            this.btn_create_user.Text = "Create user";
            this.btn_create_user.UseVisualStyleBackColor = true;
            this.btn_create_user.Click += new System.EventHandler(this.btn_create_user_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(64, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Role :";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(103, 195);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(214, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // tb_cu_Reenter
            // 
            this.tb_cu_Reenter.Location = new System.Drawing.Point(103, 115);
            this.tb_cu_Reenter.Name = "tb_cu_Reenter";
            this.tb_cu_Reenter.PasswordChar = '•';
            this.tb_cu_Reenter.Size = new System.Drawing.Size(214, 20);
            this.tb_cu_Reenter.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-4, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Re-enter Password ;";
            // 
            // form_create_user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 261);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb_cu_Reenter);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_create_user);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_cu_Surname);
            this.Controls.Add(this.tb_cu_Firstname);
            this.Controls.Add(this.tb_cu_Password);
            this.Controls.Add(this.tb_cu_Username);
            this.Name = "form_create_user";
            this.Text = "2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_cu_Username;
        private System.Windows.Forms.TextBox tb_cu_Password;
        private System.Windows.Forms.TextBox tb_cu_Surname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_create_user;
        private System.Windows.Forms.TextBox tb_cu_Firstname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox tb_cu_Reenter;
        private System.Windows.Forms.Label label7;
    }
}