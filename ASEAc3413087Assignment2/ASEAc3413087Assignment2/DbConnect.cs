﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace ASEAc3413087Assignment2
{
    /// <summary>
    /// This class handles creating the connection: Just cleans up the Main.cs a little
    /// </summary>
    public class DbConnect
    {

        public MySqlConnection connection;
        public string dbserver;
        public string dbdatabase;
        public string dbuid;
        public string dbpassword;
        /// <summary>
        /// This is the function of the class which actually handles the instantiation of the connection, connection is passed back to calling class.
        /// </summary>
        /// <returns>Returns MySqlConnection connection / allows single-submission of connection string</returns>
        public MySqlConnection CreateConnection()
        {
            dbserver = "localhost";
            dbdatabase = "assig2";
            dbuid = "root";
            dbpassword = "";
            string connectionString;
            connectionString = "SERVER=" + dbserver + ";" + "DATABASE=" +
            dbdatabase + ";" + "UID=" + dbuid + ";" + "PASSWORD=" + dbpassword + ";";
            connection = new MySqlConnection(connectionString);
            connection.Open();
            Console.WriteLine("Connection Established");
            return connection;
        }

    }
}
